package eSigns;

import java.io.IOException;
import java.util.Arrays;

import javax.xml.bind.JAXBException;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.junit.Test;
import org.junit.Assert;
import se.esigns.AppDatabase;
import se.esigns.controller.publisher.PublisherController;
import se.esigns.controller.sign.SignsController;
import se.esigns.model.commercial.Commercial;
import se.esigns.model.commercial.MediaCommercial;
import se.esigns.model.commercial.OneTimePolicy;
import se.esigns.model.commercial.TextCommercial;
import se.esigns.model.publisher.Publisher;
import se.esigns.model.sign.AreaType;
import se.esigns.model.sign.Sign;
import se.esigns.view.publisher.PublisherView;
import se.esigns.view.sign.SignView;

public class ApplicationTest {
    AppDatabase db = new AppDatabase("dbxml");
    Display display = new Display();
    
    SignsController sCtrl = new SignsController(db);
	Sign s1 = new Sign("s1", 3, "area1", AreaType.Non_Urban, false);
	Sign s2 = new Sign("s2", 3, "area1", AreaType.Non_Urban, false);
	Sign s3 = new Sign("s3", 3, "area2", AreaType.Non_Urban, false);
	Publisher p1 = new Publisher("p1", "pass");
	
	PublisherView p1View = new PublisherView(display);
	PublisherController pc1 = new PublisherController(db, p1View);
	
	Sign[] signCol1 = { s1, s3 };
	Sign[] signCol2 = { s2, s3 };
	
	Commercial c1 = new TextCommercial("c1",new OneTimePolicy(), p1.getUsername(),
		Arrays.asList(signCol1), "c1 text");
	Commercial c2 = new TextCommercial("c2",new OneTimePolicy(), p1.getUsername(),
		Arrays.asList(signCol2), "c2 text");
	MediaCommercial m1 = new MediaCommercial("c3",new OneTimePolicy(), p1.getUsername(),
		Arrays.asList(signCol2), "C:\\Studies\\software engineering\\swt.png");
	
	Commercial[] signComms1 = { c1 };
	Commercial[] signComms3 = { m1,c1, c2 };
	
    @Test
    public void simulateSign() throws JAXBException, IOException, InterruptedException {
	db.loadDatabase();
	db.addSign(s1);
	s1 = db.getSigns().get("s1");
	db.addSign(s2);
	s2 = db.getSigns().get("s2");
	db.addSign(s3);
	s3 = db.getSigns().get("s3");
	pc1.setPassword("pass");
	pc1.setUsername("p1");
	db.registerPublisher(pc1);
	db.login(pc1);
	// p1View.onLogin();
	pc1.publish(c1);
	pc1.publish(c2);
	pc1.publish(m1);
//	pc1.registerPublisherListener(p1View);
	sCtrl.startAllocation();
	Thread.sleep(1000);
	s1 = db.getSigns().get("s1");
	s2 = db.getSigns().get("s2");
	s3 = db.getSigns().get("s3");
	Assert.assertArrayEquals(signComms1,
		s1.getCommercials().toArray(new Commercial[1]));
	Assert.assertArrayEquals(signComms3,
		s3.getCommercials().toArray(new Commercial[2]));

	final SignView s1View = new SignView(display, pc1, s1);
	final SignView s3View = new SignView(display, pc1, s3);
	db.registerSignListener(s3View, "s3");
	p1View.initUI();
	s3View.simulateSign();
	
	db.saveDatabase();
	

    }

}
