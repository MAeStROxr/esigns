package eSigns;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import se.esigns.AppDatabase;
import se.esigns.model.commercial.Commercial;
import se.esigns.model.commercial.OneTimePolicy;
import se.esigns.model.commercial.TextCommercial;
import se.esigns.model.publisher.Publisher;
import se.esigns.model.sign.AreaType;
import se.esigns.model.sign.Sign;

public class AppDataTest {
    AppDatabase db = new AppDatabase("dbxml");
    Publisher p1 = new Publisher("p1", "pass");
    Sign s1 = new Sign("s1", 3, "area1", AreaType.Non_Urban, false);
    Sign s2 = new Sign("s2", 3, "area1", AreaType.Non_Urban, false);
    Sign s3 = new Sign("s3", 3, "area2", AreaType.Non_Urban, false);
    Sign[] signCol1 = { s1, s3 };
    Sign[] signCol2 = { s2, s3 };
    Commercial c1 = new TextCommercial("c1",new OneTimePolicy(), p1.getUsername(),
	    Arrays.asList(signCol1), "c1 text");
    Commercial c2 = new TextCommercial("c2",new OneTimePolicy(), p1.getUsername(),
	    Arrays.asList(signCol2), "c2 text");
    Commercial[] signComms1 = { c1 };
    Commercial[] signComms3 = { c1, c2 };
    
    Comparator sorter = new Comparator<Sign>() {

	    @Override
	    public int compare(Sign o1, Sign o2) {
		// TODO Auto-generated method stub
		return o1.getSignName().compareTo(o2.getSignName());
	    }
	};
    @Before
    public void resetData(){
	db=new AppDatabase("dbxml");
    }
    @Test
    public void addSigns() {
	db.addSign(s1);
	db.addSign(s3);
	Sign[] expecteds = {s1,s3};
	ArrayList<Sign> signs= new ArrayList(db.getSigns().values());
	Collections.sort(signs,sorter);
	Sign[] actuals = signs.toArray(new Sign[signs.size()]);
	Assert.assertArrayEquals(expecteds, actuals);
	
    }
    @Test
    public void addCommercialsToSign(){
	db.addSign(s1);
	c1.setName("Commercial:0");
	db.addCommercialToSign(c1, s1.getSignName());
	List<Commercial> comms=db.getSigns().get(s1.getSignName()).getCommercials();
	assertEquals(1, comms.size());
	assertEquals("Commercial:0",comms.get(0).getName());
	
    }

}
