package eSigns;

import static org.junit.Assert.*;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import se.esigns.AppDatabase;
import se.esigns.model.commercial.Commercial;
import se.esigns.model.commercial.MediaCommercial;
import se.esigns.model.commercial.OneTimePolicy;
import se.esigns.model.commercial.PeriodicPolicy;
import se.esigns.model.commercial.TextCommercial;
import se.esigns.model.publisher.Publisher;
import se.esigns.model.sign.AreaType;
import se.esigns.model.sign.Sign;

public class TimingTest {
    Date now;
    Date minuteFromNow;
    Date inThirtyMinutes;// 29
    Date inThirtyFiveMinutes;
    Date minuteAgo;
    Date thirtyMinutesAgo;// -29
    Date thirtyFiveMinutesAgo;
    Date dayFromNow;
    Date dayAgo;
    Date tenDaysFromNow;
    Date tenDaysAgo;
    @Before
    public void reset() {
	Calendar c = Calendar.getInstance();
	c.add(Calendar.SECOND, 1);
	now = c.getTime();//now is actually a second from now
	c.add(Calendar.MINUTE, 1);
	minuteFromNow = c.getTime();
	c.add(Calendar.MINUTE, 28);
	inThirtyMinutes=c.getTime();
	c.add(Calendar.MINUTE, 6);
	inThirtyFiveMinutes=c.getTime();
	c=Calendar.getInstance();
	c.add(Calendar.MINUTE, -1);
	minuteAgo = c.getTime();
	c.add(Calendar.MINUTE, -28);
	thirtyMinutesAgo = c.getTime();
	c.add(Calendar.MINUTE, -6);
	thirtyFiveMinutesAgo = c.getTime();
	
	c=Calendar.getInstance();
	c.add(Calendar.DAY_OF_MONTH, 1);
	dayFromNow=c.getTime();
	c.add(Calendar.DAY_OF_MONTH, 9);
	tenDaysFromNow=c.getTime();
	c.add(Calendar.DAY_OF_MONTH, -11);
	dayAgo=c.getTime();
	c.add(Calendar.DAY_OF_MONTH, -9);
	tenDaysAgo=c.getTime();
	

    }

    
    @Test
    public void oneTimePolicyExpiredTest(){
	OneTimePolicy p = new OneTimePolicy(thirtyFiveMinutesAgo);
	assertFalse(p.expired());
	assertFalse(p.timeToPublish());
	assertTrue(p.expired());
    }
    
    @Test
    public void oneTimePolicyTimeToPublishTest() {
	OneTimePolicy p = new OneTimePolicy(now);
	assertTrue(p.timeToPublish());
	p = new OneTimePolicy(minuteAgo);
	assertTrue(p.timeToPublish());
	p = new OneTimePolicy(minuteFromNow);
	assertTrue(p.timeToPublish());
	p = new OneTimePolicy(thirtyMinutesAgo);
	assertTrue(p.timeToPublish());
	p = new OneTimePolicy(inThirtyMinutes);
	assertTrue(p.timeToPublish());
	
	
	p = new OneTimePolicy(thirtyFiveMinutesAgo);
	assertFalse(p.timeToPublish());
	p = new OneTimePolicy(inThirtyFiveMinutes);
	assertFalse(p.timeToPublish());
    }

    @Test
    public void periodicPolicyExpired(){
	
	
	 PeriodicPolicy p = new PeriodicPolicy(thirtyFiveMinutesAgo,minuteAgo,1);
	 assertTrue(p.expired());
	p = new PeriodicPolicy(tenDaysAgo,dayAgo,2);
	assertTrue(p.expired());
	
	
	p = new PeriodicPolicy(tenDaysAgo,minuteAgo,5);
	assertTrue(p.expired());
	
	
	
    }
    
    @Test
    public void periodicPolicyTimeToPublishTrue(){
	PeriodicPolicy p = new PeriodicPolicy(now,inThirtyFiveMinutes,1);
	assertTrue(p.timeToPublish());
	
	p = new PeriodicPolicy(now,inThirtyFiveMinutes,0);
	assertTrue(p.timeToPublish());
	
	p = new PeriodicPolicy(now,tenDaysFromNow,5);
	assertTrue(p.timeToPublish());
	
	p = new PeriodicPolicy(now,dayFromNow,1);
	assertTrue(p.timeToPublish());
	
	p = new PeriodicPolicy(tenDaysAgo,tenDaysFromNow,5);
	assertTrue(p.timeToPublish());
	
    }
    
    @Test
    public void periodicPolicyTimeToPublishFalse(){
	PeriodicPolicy p = new PeriodicPolicy(thirtyFiveMinutesAgo,dayFromNow,1);
	assertFalse(p.timeToPublish());
	
	p = new PeriodicPolicy(thirtyFiveMinutesAgo,now,0);
	assertFalse(p.timeToPublish());
	
	p = new PeriodicPolicy(thirtyFiveMinutesAgo,tenDaysFromNow,5);
	assertFalse(p.timeToPublish());
	
	p = new PeriodicPolicy(dayAgo,tenDaysFromNow,5);
	assertFalse(p.timeToPublish());
	
	p = new PeriodicPolicy(tenDaysAgo,tenDaysFromNow,3);
	assertFalse(p.timeToPublish());
	
    }
    
    
    @Test
    public void testPolicy() throws InterruptedException {

	// ON TIME POLICY
	Date now = Calendar.getInstance().getTime();
	Calendar cal = Calendar.getInstance();
	cal.setTime(now);
	Calendar cal2 = Calendar.getInstance();
	cal2.setTime(now);
	cal2.add(Calendar.DAY_OF_YEAR, -2);
	Date twoDaysBack = cal2.getTime();
	cal.add(Calendar.HOUR, 2);
	Date oneHourFarward = cal.getTime();
	cal.add(Calendar.WEEK_OF_YEAR, 1);
	Date oneWeekFarward = cal.getTime();
	OneTimePolicy policy1 = new OneTimePolicy(now);
	OneTimePolicy policy2 = new OneTimePolicy(oneHourFarward);
	MediaCommercial myCommercial1 = new MediaCommercial("TESTCOMMERCIAL1",
		policy1, "111", new HashSet<Sign>(), "C:\\extracredits.jpg");
	MediaCommercial myCommercial2 = new MediaCommercial("TESTCOMMERCIAL2",
		policy2, "112", new HashSet<Sign>(), "C:\\extracredits.jpg");

	System.out.println(Calendar.getInstance().getTime().toString());
	Thread.sleep(400);
	assertTrue(myCommercial1.TimeToDisplay());
	assertFalse(myCommercial2.TimeToDisplay());

	// Periodic Policy
	PeriodicPolicy perPolicy1 = new PeriodicPolicy(now, oneHourFarward, 1);
	assertTrue(perPolicy1.timeToPublish());
	PeriodicPolicy perPolicy2 = new PeriodicPolicy(twoDaysBack,
		oneHourFarward, 4);
	assertFalse(perPolicy2.timeToPublish());

	perPolicy2 = new PeriodicPolicy(twoDaysBack, oneWeekFarward, 2);
	assertTrue(perPolicy2.timeToPublish());

    }

}
