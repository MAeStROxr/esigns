package se.esigns;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import se.esigns.controller.management.ManagerController;
import se.esigns.controller.management.MarketerController;
import se.esigns.controller.publisher.OfficialPublisherController;
import se.esigns.controller.publisher.PublisherController;
import se.esigns.model.commercial.Commercial;
import se.esigns.model.commercial.ICommercialDataListener;
import se.esigns.model.commercial.PeriodicPolicy;
import se.esigns.model.commercial.TextCommercial;
import se.esigns.model.management.IManagerDatabase;
import se.esigns.model.management.IManagerDatabaseListener;
import se.esigns.model.publisher.IOfficialDatabase;
import se.esigns.model.publisher.IPublisherDatabaseListener;
import se.esigns.model.publisher.IPublisherVisitor;
import se.esigns.model.publisher.OfficialPublisher;
import se.esigns.model.publisher.Publisher;
import se.esigns.model.publisher.PType;
import se.esigns.model.publisher.PublishingPrivilege;
import se.esigns.model.sign.AreaType;
import se.esigns.model.sign.ISignAllocatorDatabase;
import se.esigns.model.sign.ISignDataListener;
import se.esigns.model.sign.ISignsDatabaseListener;
import se.esigns.model.sign.Sign;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "AppDatabase")
public class AppDatabase
	implements IOfficialDatabase, IManagerDatabase, ISignAllocatorDatabase {
    @XmlTransient
    private Set<ManagerController> managerControllers = new HashSet<ManagerController>();
    @XmlTransient
    private Set<MarketerController> marketerControllers = new HashSet<MarketerController>();
    // From
    protected Map<String, Commercial> commercials = new HashMap<String, Commercial>();
    protected Map<String, Commercial> expiredCommercials = new HashMap<String, Commercial>();
    // protected Map<String, Commercial> commercials = new HashMap<String,
    // Commercial>();

    @XmlElement(name = "sign")
    protected Map<String, Sign> signs = new HashMap<String, Sign>();
    Set<String> areas = new HashSet<String>(
	    Arrays.asList("Krayot Imperia", "Winter is coming"));
    @XmlElement(name = "areanames")
    protected JaxbList<String> areaNames = new JaxbList<String>(
	    new ArrayList<String>(areas));

    @XmlTransient
    private Set<String> loggedIn = new HashSet<String>();
    @XmlElement(name = "publisher")
    private Map<String, Publisher> publishers = new HashMap<String, Publisher>();
    // private Set<Publisher> officials;
    private final String dbFileName;
    @XmlTransient
    private ArrayList<IManagerDatabaseListener> managerListeners = new ArrayList<>();
    @XmlTransient
    private ArrayList<ISignsDatabaseListener> signsListeners = new ArrayList<>();
    @XmlTransient
    private ArrayList<ICommercialDataListener> commercialListeners = new ArrayList<>();

    public AppDatabase() {
	dbFileName = "database_file";

    }

    public AppDatabase(String dbFileName) {

	this.dbFileName = dbFileName;
    }

    public void loadDatabase() throws JAXBException {
	File xmlFile = new File(dbFileName);
	if (!xmlFile.exists())
	    return;
	JAXBContext jaxbContext = JAXBContext.newInstance(AppDatabase.class,
		Publisher.class, Publisher.class, OfficialPublisher.class);
	Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
	AppDatabase db = (AppDatabase) jaxbUnmarshaller.unmarshal(xmlFile);
	this.signs = db.signs;
	this.areaNames = db.areaNames;
	this.publishers = db.publishers;
	this.commercials = db.commercials;
	this.expiredCommercials = db.expiredCommercials;
	Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
	jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
	jaxbMarshaller.marshal(this, System.out);

	System.out.println(commercials.toString());
	for (Sign s : signs.values())
	    s.startSignClock();

    }

    public void saveDatabase() throws JAXBException {
	File xmlfile = new File(dbFileName);
	for (Sign s : signs.values())
	    s.stopSignClock();
	JAXBContext jaxbContext = JAXBContext.newInstance(AppDatabase.class);
	Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
	jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
	jaxbMarshaller.marshal(this, xmlfile);
	jaxbMarshaller.marshal(this, System.out);

    }

    public void addManager(ManagerController m) {
	managerControllers.add(m);
    }

    public void addMarketer(MarketerController m) {
	marketerControllers.add(m);
    }

    @Override
    public Map<String, Sign> getSigns() {
	HashMap<String, Sign> map = new HashMap<String, Sign>();
	for (Sign s : signs.values())
	    map.put(s.getSignName(), new Sign(s));
	return map;
    }

    @Override
    public Set<String> getAreas() {
	HashSet<String> set = new HashSet<String>();
	for (String s : areas)
	    set.add(new String(s));
	return set;
    }

    // @Override
    // public boolean publish(Publisher publisher, Commercial commercial) {
    // if (loggedIn.contains(publisher)) {
    // commercials.
    // return true;
    // }
    // return false;
    // }

    @Override
    public boolean edit(PublisherController publisher, Commercial commercial,
	    Sign sign) {
	if (loggedIn.contains(getIdentifier(publisher))) {
	    Sign s = signs.get(sign.getSignName());
	    // s.removeCommercial(commercial);
	    // s.addCommercial(commercial);
	    return true;
	}
	return false;
    }

    @Override
    public void push(PublisherController publisher, Commercial commercial) {
	for (String sign : commercial.getSignesSet()) {
	    if (loggedIn.contains(getIdentifier(publisher)) && publishers.get(
		    publisher.getUsername()) instanceof OfficialPublisher) {
		PublishingPrivilege privilege = ((OfficialPublisher) publishers
			.get(publisher.getUsername())).getPrivilege();
		if (privilege.allowed(signs.get(sign)))
		    commercial.markPushed();
		commercials.put(commercial.getName(), commercial);

		signs.get(sign).pushToRuninglist(commercial);

	    }
	}

    }

    @Override
    public boolean addSign(Sign sign) {
	if (signs.containsKey(sign.getSignName())) {
	    return false;
	}
	Sign newSign = new Sign(sign);
	Sign temp = signs.put(sign.getSignName(), newSign);
	newSign.startSignClock();
	for (ISignsDatabaseListener l : signsListeners)
	    l.onSignAdded(sign);
	areas.add(sign.getAreaName());
	return true;
    }

    @Override
    public boolean removeSign(Sign sign) {
	Sign s = signs.remove(sign.getSignName());
	if (s == null)
	    return false;
	s.stopSignClock();
	for (ISignsDatabaseListener l : signsListeners)
	    l.onSignRemoved(sign);
	return true;
    }

    @Override
    public boolean editSign(Sign sign) {
	Sign s = signs.get(sign.getSignName());
	if (s == null)
	    return false;
	s.setAreaName(sign.getAreaName());
	s.setAreaType(sign.getAreaType());
	s.setCanImage(sign.getCanImage());
	for (ISignsDatabaseListener l : signsListeners)
	    l.onSignEdited(sign);
	return true;
    }

    @Override
    public boolean setSignPrice(String signName, double pricing) {
	Sign s = signs.get(signName);
	if (s == null)
	    return false;
	s.setSignPricing(pricing);
	for (ISignsDatabaseListener l : signsListeners)
	    l.onSignEdited(new Sign(s));
	return true;
    }

    @Override
    public boolean registerOfficial(String username, String password) {

	if (publishers.containsKey(username)) {
	    // TODO: insert error "userName TAKEN"::::)))
	    return false;
	}

	PublishingPrivilege p = new PublishingPrivilege();
	OfficialPublisher o = new OfficialPublisher(username, password, this,
		p);
	publishers.put(username, o);
	return true;
    }

    @Override
    public Map<String, Publisher> getRegisteredPublishers() {
	HashMap<String, Publisher> map = new HashMap<String, Publisher>();
	for (Publisher p : publishers.values()) {
	    p.accept(CopyPub.Any);
	    map.put(CopyPub.Any.copy.getUsername(), CopyPub.Any.copy);
	}
	return map;
    }

    @Override
    public Map<String, Publisher> getLoggedInPublishers() {
	HashMap<String, Publisher> map = new HashMap<String, Publisher>();
	for (String pUsername : loggedIn) {
	    Publisher p = publishers.get(pUsername);
	    p.accept(CopyPub.Any);
	    map.put(CopyPub.Any.copy.getUsername(), CopyPub.Any.copy);
	}
	return map;
    }

    @Override
    public void updatePublisherStatistics(String publisher, String commercial,
	    String displayedIn, int secondsDisplayed, int chargedAmount) {
	if (publishers.containsKey(publisher)) {
	    Publisher p = publishers.get(publisher);
	    p.getPublisherStatistics().addDisplayTime(commercial, displayedIn,
		    secondsDisplayed, chargedAmount);
	    p.setBalance(p.getBalance()+chargedAmount);
	    p.sendStatistics();
	}
    }

    public boolean isRegistered(String username, String password) {
	return publishers.containsKey(username);
    }

    @Override
    public boolean isLoggedIn(PublisherController publisher) {
	return loggedIn.contains(getIdentifier(publisher));
    }

    private boolean validateUser(String username, String password) {
	Publisher p = publishers.get(username);
	if (p == null || !p.getPassword().equals(password))
	    return false;
	return true;
    }

    public boolean isLoggedIn(String username, String password) {
	return loggedIn.contains(getIdentifier(username, password));
    }

    private String getIdentifier(String username, String password) {
	return username + password;
    }

    private String getIdentifier(PublisherController publisher) {
	return getIdentifier(publisher.getUsername(), publisher.getPassword());
    }

    @Override
    public boolean login(PublisherController publisher) {
	if (!publishers.containsKey(publisher.getUsername())
		|| !validateUser(publisher.getUsername(),
			publisher.getPassword()))
	    return false;
	return loggedIn.add(getIdentifier(publisher));

    }

    @Override
    public boolean logout(PublisherController publisher) {
	if (!publishers.containsKey(publisher.getUsername()))
	    return false;
	return loggedIn.remove(getIdentifier(publisher));
    }

    @Override
    public boolean registerPublisher(PublisherController publisher) {
	if (publishers.containsKey(publisher.getUsername()))
	    return false;
	Publisher p = new Publisher(publisher.getUsername(),
		publisher.getPassword());

	publishers.put(p.getUsername(), p);
	p.accept(CopyPub.Any);
	Publisher newP = CopyPub.Any.copy;
	for (IManagerDatabaseListener l : managerListeners) {
	    l.onPublisherAdded(newP);
	}
	return true;
    }

    private int commercialCounter = 0;

    @Override
    public boolean publish(PublisherController publisherController,
	    Commercial commercial) {
	// commercial.setId("Commercial:" + commercialCounter++);
	if (commercials.containsKey(commercial.getName()))
	    return false;
	commercials.put(commercial.getName(), commercial);
	Publisher p = publishers.get(publisherController.getUsername());
	p.addCommercial(commercial);
	IPublisherDatabaseListener l = p.getListener();
	if (l != null)
	    l.onCommercialAdded(commercial);
	return true;
    }

    @Override
    public Publisher getPublisher(String username, String password) {
	if (!validateUser(username, password))
	    return null;
	Publisher p = publishers.get(username);
	p.accept(CopyPub.Any);
	return CopyPub.Any.copy;
    }

    @Override
    public boolean pushText(OfficialPublisherController publisher,
	    TextCommercial commercial, Sign sign) {
	// commercial.setId("Commercial:" + commercialCounter++);
	Publisher p = publishers.get(publisher.getUsername());
	if (loggedIn.contains(getIdentifier(publisher))
		&& PType.Any.visit(p) == PType.OFFICIAL) {
	    PublishingPrivilege privilege = ((OfficialPublisher) p)
		    .getPrivilege();
	    if (privilege.allowed(sign)) {
		sign.pushToRuninglist(commercial);
		return true;
	    }
	}
	return false;
    }

    @Override
    public OfficialPublisher getOfficialPublisher(String username,
	    String password) {
	if (!validateUser(username, password))
	    return null;
	Publisher p = publishers.get(username);
	if (p == null || !(p instanceof OfficialPublisher))
	    return null;
	return new OfficialPublisher(p);
    }

    @Override
    public boolean promoteOfficial(String username) {
	Publisher p = publishers.get(username);
	if (p == null || PType.Any.visit(p) == PType.OFFICIAL)
	    return false;

	OfficialPublisher op = new OfficialPublisher(p);
	publishers.remove(username);
	publishers.put(username, op);

	for (IManagerDatabaseListener l : managerListeners)
	    l.onPublisherEdited(op);
	IPublisherDatabaseListener l = p.getListener();
	if (l != null) {
	    p.unregisterPublisherDataListener();
	    op.registerPublisherDataListener(l);
	    l.onPublisherModelUpdate(op);
	}
	return true;

    }

    @Override
    public void demoteOfficial(String username) {
	Publisher p = publishers.get(username);
	if (p == null)
	    return;
	publishers.remove(username);

	Publisher newP = new Publisher(p);
	publishers.put(username, newP);
	for (IManagerDatabaseListener l : managerListeners)
	    l.onPublisherEdited(new Publisher(newP));
	IPublisherDatabaseListener l = p.getListener();
	if (l != null) {
	    p.unregisterPublisherDataListener();
	    newP.registerPublisherDataListener(l);
	    l.onPublisherModelUpdate(newP);
	}
    }

    @Override
    public boolean removePublisher(String username) {
	Publisher p = publishers.remove(username);
	if (p == null)
	    return false;
	for (IManagerDatabaseListener l : managerListeners) {
	    l.onPublisherRemoved(p);
	}
	IPublisherDatabaseListener l = p.getListener();
	if (l != null) {
	    l.onNotifyPublisher("Sorry user has been removed.");
	    l.onPublisherModelUpdate(null);
	    p.unregisterPublisherDataListener();
	}
	return true;
    }

    private enum CopyPub implements IPublisherVisitor<Publisher> {
	Any;
	public Publisher copy;

	@Override
	public Publisher visit(Publisher p) {
	    copy = new Publisher(p);
	    return copy;
	}

	@Override
	public Publisher visit(OfficialPublisher p) {
	    copy = new OfficialPublisher(p);
	    return copy;
	}

	Publisher copy(Publisher p) {
	    p.accept(this);
	    return copy;
	}

    }

    @Override
    public Publisher getPublisher(String username) {
	Publisher p = publishers.get(username);
	return CopyPub.Any.copy(p);
    }

    @Override
    public void clearCommercialsFromSign(String signName) {
	Sign s = signs.get(signName);
	if (s == null)
	    return;
	s.cleanRunList();
    }

    public final int displayTime = 10;

    @Override
    public boolean addCommercialToSign(Commercial c, String signName) {
	Sign s = signs.get(signName);
	if (s == null)
	    return false;
	s.addCommercialToRunList(c);
	Publisher p = publishers.get(c.getPublisherName());
	return true;
    }

    @Override
    public Map<String, Commercial> getCommercials(PublisherController pc) {
	if (!loggedIn.contains(getIdentifier(pc)))
	    return null;
	Publisher p = publishers.get(pc.getUsername());
	return p.getCommercials();
    }

    @Override
    public Map<String, Commercial> getCommercials() {
	return commercials;
    }

    @Override
    public void moveCommercialToExpired(Commercial c) {
	Commercial comm = commercials.remove(c.getName());
	expiredCommercials.put(comm.getName(), comm);

    }

    @Override
    public void registerSignListener(ISignDataListener signView,
	    String signName) {
	Sign s = signs.get(signName);
	if (s == null)
	    return;
	s.addListener(signView);
    }

    @Override
    public void unregisterSignListener(ISignDataListener signView,
	    String signName) {
	Sign s = signs.get(signName);
	if (s == null)
	    return;

    }

    @Override
    public void notifyPublisher(String username, String message) {
	for (Publisher p : publishers.values())
	    p.notifyPublisher(message);
    }

    @Override
    public void registerManagerDataListener(IManagerDatabaseListener listener,
	    String username, String password) {
	managerListeners.add(listener);
	signsListeners.add(listener);
    }

    @Override
    public void unregisterManagerDataListener(IManagerDatabaseListener listener,
	    String managerUsername, String managerPassword) {
	managerListeners.remove(listener);
	signsListeners.remove(listener);

    }

    @Override
    public void registerPublisherDataListener(
	    IPublisherDatabaseListener listener, String username,
	    String password) {
	if (!validateUser(username, password))
	    return;
	Publisher p = publishers.get(username);
	p.registerPublisherDataListener(listener);
	signsListeners.add(listener);
    }

    @Override
    public void unregisterPublisherDataListener(
	    IPublisherDatabaseListener listener, String username) {
	Publisher p = publishers.get(username);
	if (p == null)
	    return;
	p.unregisterPublisherDataListener();
	signsListeners.remove(listener);
    }

    @Override
    public void registerSignsListener(ISignsDatabaseListener signsView) {
	signsListeners.add(signsView);
    }

    @Override
    public void unregisterSignsListener(ISignsDatabaseListener signsView) {
	signsListeners.remove(signsView);
    }

    @Override
    public boolean addAreaTypePrivilege(AreaType areaType, String username) {
	Publisher p =publishers.get(username);
	if (p == null || !p.isOfficial())
	    return false;
	OfficialPublisher op = (OfficialPublisher)p;
	op.getPrivilege().areaTypes.add(areaType);
	for (IManagerDatabaseListener l : managerListeners)
	    l.onAreaTypePrivilegeAdded(areaType, op.getUsername());
	return true;
    }
    
    @Override
    public boolean removeAreaTypePrivilege(AreaType areaType, String username) {
	Publisher p =publishers.get(username);
	if (p == null || !p.isOfficial())
	    return false;
	OfficialPublisher op = (OfficialPublisher)p;
	op.getPrivilege().areaTypes.remove(areaType);
	for (IManagerDatabaseListener l : managerListeners)
	    l.onAreaTypePrivilegeRemoved(areaType, op.getUsername());
	return true;
    }

    @Override
    public boolean addLocationPrivilege(String location, String username) {
	Publisher p =publishers.get(username);
	if (p == null || !p.isOfficial())
	    return false;
	OfficialPublisher op = (OfficialPublisher)p;
	op.getPrivilege().areaNames.add(location);
	for (IManagerDatabaseListener l : managerListeners)
	    l.onLocationPrivilegeAdded(location, op.getUsername());
	return true;
    }

    @Override
    public boolean removeLocationPrivilege(String location, String username) {
	Publisher p =publishers.get(username);
	if (p == null || !p.isOfficial())
	    return false;
	OfficialPublisher op = (OfficialPublisher)p;
	op.getPrivilege().areaNames.remove(location);
	for (IManagerDatabaseListener l : managerListeners)
	    l.onLocationPrivilegeRemoved(location, op.getUsername());
	return true;
    }

   

}
