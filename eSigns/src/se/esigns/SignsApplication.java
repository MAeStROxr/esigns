package se.esigns;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

import javax.xml.bind.JAXBException;

import org.eclipse.swt.widgets.Display;


import se.esigns.controller.management.ManagerController;
import se.esigns.controller.management.MarketerController;
import se.esigns.controller.publisher.PublisherController;
import se.esigns.controller.sign.SignsController;
import se.esigns.model.publisher.IPublisherDatabase;
import se.esigns.view.management.ManagerView;
import se.esigns.view.management.MarketerView;
import se.esigns.view.publisher.PublisherView;

public class SignsApplication {
    /*
     * STATE
     */
    private final String appDataFile = "AppDatabase.xml";
    // All application databases are inside AppDatabase
    // Signs,ManagerController,MarketerController,Publisher
    private AppDatabase appData = new AppDatabase(appDataFile);
    private SignsController signsController = new SignsController(appData);

    /*
     * ====================================================================
     * BEHAVIOR
     */
    public void clear() {
	File f = new File(appDataFile);
	if (f.exists())
	    f.delete();
    }

    public void start() throws JAXBException {
	appData.loadDatabase();
	signsController.startAllocation();
    }

    public void close() throws JAXBException {
	signsController.stopAllocation();
	appData.saveDatabase();
    }

    /**
     * 
     * @param username
     * @param password
     * @param signsInchargeOf
     * @param registerOfficial
     */
    public ManagerController createManager(Display display) {
	ManagerView v = new ManagerView(display);
	ManagerController m = new ManagerController(appData, v);
	appData.addManager(m);
	return m;
    }

    public MarketerController createMarketer(Display display) {
	MarketerView v = new MarketerView(display);
	MarketerController m = new MarketerController(appData, v);
	appData.addMarketer(m);
	return m;
      }
    
    public PublisherController createPublisher(Display display) {
	PublisherView v = new PublisherView(display);
	PublisherController p = new PublisherController(appData, v);
	return p;
    }

    

    public IPublisherDatabase getPublisherDB() {
	return appData;
    }

    public void simulateSigns() {
	// TODO - implement SignsApplication.simulteSigns}
    }

  

  

  

}