package se.esigns.model.publisher;

import se.esigns.model.commercial.ICommercialDataListener;
import se.esigns.model.sign.AreaType;
import se.esigns.model.sign.ISignsDatabaseListener;

public interface IPublisherDatabaseListener extends ICommercialDataListener, ISignsDatabaseListener{
    public void onPublisherModelUpdate(Publisher publisher);
    public void onPublisherStatsUpdate(Statistics stats,Publisher publisher);
    public void onNotifyPublisher(String message);
   
    
}
