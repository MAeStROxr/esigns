package se.esigns.model.publisher;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "DisplayCharge")
public class SignStats {
    public int signDisplayTime;
    public int signCharge;
    public String toString(){
	return "signDisplayTime: "+signDisplayTime+" signCharge: "+signCharge+"\n";
    }
    public SignStats() {
    }
}