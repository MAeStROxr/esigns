package se.esigns.model.publisher;

import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import se.esigns.model.commercial.Commercial;
import se.esigns.model.sign.Sign;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Statistics")
public class Statistics {

	public int accumulatedTime=0;
	int overallCharge=0;
	public HashMap<String, CommercialStats> commercialsPublished = new HashMap<String, CommercialStats>();

	public Statistics(Statistics stats) {
	    this.accumulatedTime=stats.accumulatedTime;
	    this.overallCharge=stats.overallCharge;
	    this.commercialsPublished.putAll(stats.commercialsPublished);
	}

	public Statistics() {
	    // TODO Auto-generated constructor stub
	}

	@Override
	public String toString(){
	    StringBuffer b = new StringBuffer();
	    b.append("accumulatedTime: "+accumulatedTime+" overallCharge: ").
	    append(overallCharge+"\n").append(commercialsPublished.toString()).append("\n");
	    return b.toString();
	}
	
	public void addDisplayTime(String commercialId, String signName,
			int secondsDisplayed, int chargedAmount) {
		if (!commercialsPublished.containsKey(commercialId)) {
			commercialsPublished.put(commercialId, new CommercialStats());

		}
		CommercialStats s = commercialsPublished.get(commercialId);
		if (s == null)
			return;
		s.displayTime += secondsDisplayed;
		s.charged += chargedAmount;
		SignStats t = new SignStats();
		t.signCharge = chargedAmount;
		t.signDisplayTime = secondsDisplayed;
		SignStats oldVal = s.publishedInSigns.putIfAbsent(signName, t);
		if (oldVal != null) {
			oldVal.signCharge += chargedAmount;
			oldVal.signDisplayTime += secondsDisplayed;
			s.publishedInSigns.put(signName, oldVal);
		}

		overallCharge += chargedAmount;
		accumulatedTime += secondsDisplayed;
	}

}