package se.esigns.model.publisher;

import java.util.HashMap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import se.esigns.model.sign.Sign;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "CommercialStats")
public class CommercialStats {
	public int displayTime;
	public int charged;
	public HashMap<String, SignStats> publishedInSigns = new HashMap<String, SignStats>();

	public CommercialStats() {
	}
	@Override
	public String toString(){
	    StringBuffer b = new StringBuffer();
	    b.append("displayTime: "+displayTime+" charged: ").
	    append(charged+"\n").append(publishedInSigns.toString()).append("\n");
	    return b.toString();
	}
}