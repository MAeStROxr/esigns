package se.esigns.model.publisher;

import java.util.HashMap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import se.esigns.model.commercial.Commercial;
import se.esigns.model.sign.Sign;
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "PublisherStatistics")
public class PublisherStatistics {
    
    public int accumulatedTime;
    int overallCharge;
    HashMap<Commercial, CommercialStats> commercialsPublished=new HashMap<Commercial, CommercialStats> ();

    public void addCommercial(Commercial commercial) {

	CommercialStats s= new CommercialStats();
	commercialsPublished.put(commercial, s);
    }

    public void addDisplayTime(String commercial,String displayedIn, int secondsDisplayed,
	     int chargedAmount) {
	if (!commercialsPublished.containsKey(commercial)) {
	    return;
	}
	CommercialStats s = commercialsPublished.get(commercial);
	if (s==null) return;
	s.displayTime += secondsDisplayed;
	s.charged += chargedAmount;
	SignStats t=new SignStats();
	t.signCharge=chargedAmount;
	t.signDisplayTime=secondsDisplayed;
	SignStats oldVal = s.publishedInSigns.putIfAbsent(displayedIn, t);
	if (oldVal!=null) {
	    oldVal.signCharge+=chargedAmount;
	    oldVal.signDisplayTime+=secondsDisplayed;
	    s.publishedInSigns.put(displayedIn, oldVal);
	}
	
	overallCharge+=chargedAmount;
	accumulatedTime+=secondsDisplayed;
    }
    
    

}