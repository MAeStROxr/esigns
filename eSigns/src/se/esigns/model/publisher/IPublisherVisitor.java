package se.esigns.model.publisher;

public interface IPublisherVisitor<T> {
    public T visit(Publisher p);

    public T visit(OfficialPublisher p);
}
