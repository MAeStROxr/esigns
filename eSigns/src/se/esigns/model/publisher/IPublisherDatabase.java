package se.esigns.model.publisher;

import java.util.List;
import java.util.Map;
import java.util.Set;

import se.esigns.controller.publisher.PublisherController;
import se.esigns.model.commercial.Commercial;
import se.esigns.model.commercial.TextCommercial;
import se.esigns.model.sign.ISignDataListener;
import se.esigns.model.sign.ISignsDatabase;
import se.esigns.model.sign.Sign;

public interface IPublisherDatabase extends ISignsDatabase{

 
    public boolean publish(PublisherController publisherController,
	    Commercial commercial);

    public boolean edit(PublisherController publisher, Commercial commercial,
	    Sign sign);

    public boolean isLoggedIn(PublisherController publisher);

    public boolean login(PublisherController publisherController);

    public boolean logout(PublisherController publisherController);

    public boolean registerPublisher(PublisherController publisherController);

    public Publisher getPublisher(String username, String password);

    public void registerPublisherDataListener(
	    IPublisherDatabaseListener listener,
	    String username, String password);

    public void unregisterPublisherDataListener(
	    IPublisherDatabaseListener listener,
	    String username);

    public Map<String, Commercial> getCommercials(PublisherController publisherController);

    public void push(PublisherController publisherController,
	    Commercial publishable_commercial);

}
