package se.esigns.model.publisher;





import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "OFFICIAL")
public class OfficialPublisher extends Publisher {
	PublishingPrivilege privilege=new PublishingPrivilege();

	public PublishingPrivilege getPrivilege() { return privilege;
	}
	
	public OfficialPublisher() {
	    // TODO Auto-generated constructor stub
	}

	
//	@Override
//	public void publish(Commercial commercial, Set<Sign> publishedInSigns) {
//	    if (commercial instanceof TextCommercial){
//		pushText((TextCommercial)commercial, publishedInSigns);
//	    }
//	}

	/**
	 * 
	 * @param username
	 * @param password
	 * @param officialsData
	 */
	public OfficialPublisher(String username, String password, IOfficialDatabase officialsData,
			PublishingPrivilege privilege) {
		super(username,password);
		
		this.privilege=privilege;
		
		
	}

	public OfficialPublisher(Publisher p) {
	   super(p);
	}

	public void setPrivilege(PublishingPrivilege privilege) {
		this.privilege=privilege;
		
	}
	
	public <T> void accept(IPublisherVisitor<T> visitor){
	    visitor.visit(this);
	}
	
	@Override
	public boolean equals(Object o){
	    if (o==null) return false;
	    if (o instanceof OfficialPublisher) {
		OfficialPublisher oPub=(OfficialPublisher)o;
		return (super.equals(o) && privilege.equals(oPub.getPrivilege()));
	    }
	    return false;
	}
}