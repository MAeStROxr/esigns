package se.esigns.model.publisher;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlTransient;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import se.esigns.model.commercial.Commercial;
import se.esigns.model.commercial.MediaCommercial;
import se.esigns.model.commercial.PeriodicPolicy;
import se.esigns.model.commercial.TextCommercial;
import se.esigns.model.sign.Sign;

@XmlSeeAlso({ OfficialPublisher.class })
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Publisher")
public class Publisher {
    @XmlTransient
    private IPublisherDatabaseListener listener;
    public HashMap<String, Commercial> publishedCommercials = new HashMap<String, Commercial>();
    public double balance=0;
    private Statistics stats = new Statistics();
    private String username, password;

    /**
     * 
     * @param username
     * @param password
     * @param officialsData
     */

    public Publisher(String username, String password) {
	this.username = username;
	this.password = password;

    }

    public Publisher(Publisher p) {
	this.username = p.username;
	this.password = p.password;
	this.publishedCommercials=p.publishedCommercials;
	this.stats=p.stats;
	this.listener=p.listener;
	this.balance=p.balance;

    }

    public Publisher() {
	username = "Publisher";
	password = "";

    }

    public void registerPublisherDataListener(
	    IPublisherDatabaseListener listener) {
	this.listener=listener;
    }

    public void unregisterPublisherDataListener() {
	this.listener=null;

    }
    public IPublisherDatabaseListener getListener(){
	return listener;
    }
    public <T> void accept(IPublisherVisitor<T> visitor) {
	visitor.visit(this);
    }

    public Statistics getPublisherStatistics() {
	return stats;
    }

    public void sendStatistics(){
	if (listener!=null)
	    listener.onPublisherStatsUpdate(stats, this);
    }
    public void notifySignRemoved(Sign sign) {
	//publishedInSigns.remove(sign);
    }

    public String getUsername() {
	return username;
    }

    public String getPassword() {
	return password;
    }

    @Override
    public boolean equals(Object otherPublisher) {
	Publisher oPub = (Publisher) otherPublisher;
	return (username.equals(oPub.getUsername())
		&& password.equals(oPub.getPassword()));
    }

    @Override
    public String toString() {
	return new StringBuffer(" Publisher Username : ").append(this.username)
		.append(" Publisher Password : ").append(this.password)
		.toString();
    }

    
    public double getBalance(){
	return balance;
    }
    public void setBalance(double balance){
	this.balance=balance;
    }
    
    public void addCommercial(Commercial c) {
	publishedCommercials.put(c.getName(), c);
    }
    public Map<String,Commercial> getCommercials(){
	return publishedCommercials;
    }

    public void notifyPublisher(String message) {
	if (listener!=null)
	    listener.onNotifyPublisher(message);
    }

    public boolean isOfficial() {
	 accept(PType.Any);
	 return	PType.lastType==PType.OFFICIAL;
    }
}