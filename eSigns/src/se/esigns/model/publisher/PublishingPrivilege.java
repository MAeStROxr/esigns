package se.esigns.model.publisher;

import java.util.HashSet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import se.esigns.model.sign.AreaType;
import se.esigns.model.sign.Sign;
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "PublishingPrivilege")
public class PublishingPrivilege {
	public HashSet<AreaType> areaTypes=new HashSet<AreaType>();
	public HashSet<String> areaNames=new HashSet<String>();
	public PublishingPrivilege(){
	    
	}
	public PublishingPrivilege(HashSet<AreaType> areaTypes,HashSet<String> areaNames ){
		this.areaTypes=areaTypes;
		this.areaNames=areaNames;
		
	}
	public boolean allowed(Sign sign) {
//		if (areaTypes.contains(sign.getAreaType()) && areaNames.contains(sign.getAreaName()))
//			return true;
//		return false;
	    return true;
	}
	
}
