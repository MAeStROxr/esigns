package se.esigns.model.publisher;

public enum PType implements IPublisherVisitor<PType> {
    Any,
    Publisher,
    OFFICIAL;
    ;
    static public PType lastType;
    @Override
    public PType visit(Publisher p) {
	// TODO Auto-generated method stub
	lastType=Publisher;
	return Publisher;
    }
    
    @Override 
    public PType visit(OfficialPublisher p){
	lastType=OFFICIAL;
	return OFFICIAL;
    }
 
    public PType getType(Publisher p){
	p.accept(this);
	return lastType;
    }

}
