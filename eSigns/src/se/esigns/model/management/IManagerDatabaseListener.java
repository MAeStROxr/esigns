package se.esigns.model.management;

import se.esigns.model.publisher.OfficialPublisher;
import se.esigns.model.publisher.Publisher;
import se.esigns.model.publisher.PublishingPrivilege;
import se.esigns.model.sign.AreaType;
import se.esigns.model.sign.ISignsDatabaseListener;
import se.esigns.model.sign.Sign;

public interface IManagerDatabaseListener extends ISignsDatabaseListener {

    public void onPublisherPrivilegeEdited( PublishingPrivilege pp, Publisher publisher);
    public void onPublisherAdded(Publisher publisher);
    public void onPublisherRemoved(Publisher publisher);
    public void onPublisherEdited(Publisher p);
    public void onLocationPrivilegeAdded(String location, String username);
    public void onLocationPrivilegeRemoved(String location, String username);
    public void onAreaTypePrivilegeAdded(AreaType areaType, String username);
    public void onAreaTypePrivilegeRemoved(AreaType areaType, String username);
}
