package se.esigns.model.management;

import se.esigns.model.publisher.OfficialPublisher;
import se.esigns.model.sign.AreaType;
import se.esigns.model.sign.ISignsDatabase;
import se.esigns.model.sign.Sign;

public interface IManagerDatabase
	extends IMarketingDatabase {
   

    boolean addSign(Sign sign);

    boolean removeSign(Sign sign);
    
    public boolean registerOfficial(String username, String password);

    public boolean promoteOfficial(String username);

    public boolean removePublisher(String username);

    public void demoteOfficial(String username);

    public void registerManagerDataListener(IManagerDatabaseListener listener,
	    String username, String password);
    
    public void unregisterManagerDataListener(IManagerDatabaseListener listener,
	    String managerUsername, String managerPassword);

    public boolean editSign(Sign sign);
    
    public boolean addLocationPrivilege(String location,String username);
    
    public boolean addAreaTypePrivilege(AreaType areaType,String username);
    
    public boolean removeLocationPrivilege(String location,String username);
    
    public boolean removeAreaTypePrivilege(AreaType areaType,String username);
    
    
    

}
