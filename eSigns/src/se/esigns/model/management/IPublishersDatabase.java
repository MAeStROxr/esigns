package se.esigns.model.management;

import java.util.List;
import java.util.Map;

import se.esigns.controller.publisher.PublisherController;
import se.esigns.model.publisher.IPublisherDatabaseListener;
import se.esigns.model.publisher.OfficialPublisher;
import se.esigns.model.publisher.Publisher;
import se.esigns.model.sign.Sign;

public interface IPublishersDatabase {
   

   

    public Publisher getPublisher(String username);
    
    public Map<String, Publisher> getRegisteredPublishers();

    public Map<String, Publisher> getLoggedInPublishers();
    
}