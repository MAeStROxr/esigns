package se.esigns.model.management;

import java.util.Map;

import se.esigns.model.commercial.ICommercialDataListener;
import se.esigns.model.publisher.Publisher;
import se.esigns.model.sign.ISignAllocatorDatabase;
import se.esigns.model.sign.ISignsDatabase;

public interface IMarketingDatabase extends ISignsDatabase , IPublishersDatabase, ISignAllocatorDatabase {
	public boolean setSignPrice(String signName, double pricing);

	public void notifyPublisher(String username,
		String message);
	
	
	
	
}
