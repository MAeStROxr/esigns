package se.esigns.model.commercial;

import se.esigns.model.sign.ISignDataListener;

public interface ICommercialDataListener  {
    public void onCommercialAdded(Commercial commercial);
    public void onCommercialRemoved(Commercial commercial);
   
}
