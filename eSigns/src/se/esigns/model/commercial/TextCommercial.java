package se.esigns.model.commercial;

import java.util.Collection;
import java.util.Set;

import javax.xml.bind.annotation.XmlRootElement;

//import org.eclipse.swt.SWT;
//import org.eclipse.swt.events.PaintEvent;
//import org.eclipse.swt.events.PaintListener;
//import org.eclipse.swt.graphics.Font;
//import org.eclipse.swt.graphics.GC;
//import org.eclipse.swt.widgets.Display;
//import org.eclipse.swt.widgets.MessageBox;
//import org.eclipse.swt.widgets.Shell;

import se.esigns.model.publisher.Publisher;
import se.esigns.model.sign.Sign;
@XmlRootElement(name = "Commercial")
public class TextCommercial extends Commercial {

	private String text;
	public TextCommercial(){}
    public TextCommercial(String commercialName,Policy policy,
	    String publisher,Collection<Sign> collection,String text) {
	super(commercialName,policy, publisher, collection);
	this.text = text;
    }
    public String getText() {
	return text;
    }
}
