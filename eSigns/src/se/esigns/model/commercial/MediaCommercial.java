package se.esigns.model.commercial;

import java.util.Collection;
import java.util.Set;

import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.ImageLoader;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.printing.PrintDialog;
import org.eclipse.swt.printing.Printer;
import org.eclipse.swt.printing.PrinterData;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

import se.esigns.model.publisher.Publisher;
import se.esigns.model.sign.Sign;
@XmlRootElement(name = "Commercial")
public class MediaCommercial extends Commercial {
    private Image image;
    private String image_adress;
    public MediaCommercial(){}
    public MediaCommercial(String commercialName,Policy policy, String publisher,
	    Collection<Sign> collection,String imageFile) {
	super(commercialName,policy, publisher, collection);
	ImageLoader loader = new ImageLoader();
	if (imageFile == null) {
	}
	image_adress = imageFile;
	ImageData[] imageData = loader.load(imageFile);
	if (imageData.length <= 0)
	    throw new RuntimeException();
    }
    public Image getImage() {
	return image;
    }
    public String getImageAdress() {
    	return image_adress;
        }
}
