package se.esigns.model.commercial;

import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement(name = "Policy")
public class PeriodicPolicy extends Policy {

	
	Date endDate;
	int daysIntervals;

	public PeriodicPolicy() {
		this.endDate=Calendar.getInstance().getTime();
		published = false;
	}
	
	public PeriodicPolicy(Date startDate, Date EndDate,int Interval) {

		this.startDate = startDate;
		this.endDate = EndDate;
		this.daysIntervals=Interval;
	}
	
	
	@SuppressWarnings("deprecation")
	@Override
	public boolean timeToPublish() {
		Date now = Calendar.getInstance().getTime();
		if (now.after(endDate)){
		    this.published=true;
		    return false;
		}
		Calendar nowC = Calendar.getInstance();
		Calendar startDate = Calendar.getInstance();
		startDate.setTime(this.startDate);
		int daysDifference = nowC.get(Calendar.DAY_OF_YEAR)-startDate.get(Calendar.DAY_OF_YEAR);
		if (daysIntervals>1 && daysDifference%daysIntervals!=0)
		    return false;
		startDate.add(Calendar.MINUTE, -30);
		startDate.set(Calendar.DAY_OF_YEAR, nowC.get(Calendar.DAY_OF_YEAR));
		if (now.before(startDate.getTime()))
		    return false;
		startDate.add(Calendar.MINUTE, 60);
		if (now.after(startDate.getTime()))
		    return false;
		return true;
	}

	@Override
	public boolean expired() {
		if (Calendar.getInstance().getTime().after(endDate))
			return true;
		return false;
	}
}
