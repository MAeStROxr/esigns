package se.esigns.model.commercial;

import java.util.Calendar;
import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement(name = "Policy")
public class OneTimePolicy extends Policy {

	public OneTimePolicy() {
		this.startDate=Calendar.getInstance().getTime();
		published = false;
	}
	
	public OneTimePolicy(Date start) {
		this.startDate=start;
		published = false;
	}

	public boolean getpublishState() {
		return published;
	}
	@Override
	public boolean timeToPublish() {
		Date now = Calendar.getInstance().getTime();
		Calendar low= Calendar.getInstance();
		Calendar high = Calendar.getInstance();
		low.setTime(this.startDate);
		low.add(Calendar.MINUTE,-30);
		high.setTime(this.startDate);
		high.add(Calendar.MINUTE,30);
		Date startTime=low.getTime();
		Date endTime = high.getTime();
		if (now.after(startTime)&&now.before(endTime)){
			return true;
		}
		if (now.after(endTime))
			this.published=true;
		return false;
	}

	@Override
	public boolean expired() {
		return published;
	}
}
