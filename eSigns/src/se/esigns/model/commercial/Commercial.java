package se.esigns.model.commercial;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
//import java.util.Date;
import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlTransient;

//import javax.xml.bind.annotation.XmlAccessType;
//import javax.xml.bind.annotation.XmlAccessorType;
//import javax.xml.bind.annotation.XmlRootElement;
//import javax.xml.bind.annotation.XmlTransient;
//
//import org.eclipse.swt.widgets.Shell;

import se.esigns.model.publisher.Publisher;
import se.esigns.model.sign.Sign;


//@XmlRootElement(name = "Commercial")
//@XmlTransient //Prevents the mapping of a JavaBean property/type to XML representation
//@XmlTransient
@XmlAccessorType(XmlAccessType.FIELD)
@XmlSeeAlso({TextCommercial.class, MediaCommercial.class})
public abstract class Commercial {

	public Policy policy;
	
	private String commercialName;
	private String publisherName;
	private Set<String> SignesRequsted=new HashSet<String>();
	private boolean pushed=false;
	
	// C'tor
	public Commercial(){}
	public Commercial(String commercialName, Policy policy, String publisher, Collection<Sign> collection) {
		this.policy = policy;
		this.commercialName=commercialName;
		this.publisherName = publisher;
		this.SignesRequsted = new HashSet<String>();
		for (Sign s : collection)
		    this.SignesRequsted.add(s.getSignName());
	}	
	
	public void setName(String id){
	    this.commercialName=id;
	}
	

	public boolean expired() {
		return this.policy.expired();
	}

	public boolean TimeToDisplay() {
		return this.policy.timeToPublish();
	}

	public Set<String> getSignesSet() {
		return this.SignesRequsted;
	}

	public String getName() {
		return commercialName;
	}
//
//	public Publisher getPublisher() {
//		return publisher;
//	}
	public String getPublisherName() {
		return publisherName;
	}

	public Policy getPublishingPolicy() {
		return policy;
	}
	public void markPushed(){
	    pushed=true;
	}
	public boolean isPushed(){
	    return pushed;
	}
	
}