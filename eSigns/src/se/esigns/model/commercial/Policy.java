package se.esigns.model.commercial;

import java.util.Calendar;
import java.util.Date;

import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlTransient;
@XmlTransient
@XmlSeeAlso({OneTimePolicy.class, PeriodicPolicy.class})
public abstract class Policy {
	protected boolean published;
	public void publish() {
		published = true;
	}
    
	public Date startDate;
	public abstract boolean timeToPublish();
	public abstract boolean expired();
	public Policy(){
	    startDate=Calendar.getInstance().getTime();
	}
	public Date getStartDate(){
	    return startDate;
	}
}
