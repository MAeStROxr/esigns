package se.esigns.model.sign;

import java.util.*;
import java.util.Map.Entry;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import se.esigns.model.commercial.Commercial;
import se.esigns.model.commercial.TextCommercial;
import se.esigns.model.publisher.Statistics;

/**
 * 
 * @author NIr SIGN:: sign is the packge that manage the sign to activate create
 *         instance of Sign : Sign x and provoke x.run() the sign will print it
 *         the relevant massges due to the commercials policy.
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "sign")
public class Sign {
    // Constants
    @XmlTransient
    ArrayList<ISignDataListener> listeners = new ArrayList<>();
    // private int _STATISTICS_INTERVAL_SEC=240;
    private int _COMERCIAL_DURATION_SEC = 3;
    //
    // STATE
    private final String name;
    private double pricing;
    private String areaName;
    private AreaType areaType;
    private boolean CanShowImage;
    @XmlTransient
    Thread signClock= new Thread(new Runnable() {
	    @Override
	    public void run() {
		int loopCount = 0;
		while (active) {
		    Date d1 = Calendar.getInstance().getTime();
		    try {
			runCycle();
			Thread.sleep(_COMERCIAL_DURATION_SEC * 1000);
		    } catch (InterruptedException e) {
			e.printStackTrace();
		    }
		    Date d2 = Calendar.getInstance().getTime();
		    long seconds = (d2.getTime() - d1.getTime()) / 1000;
		    // System.out.println(loopCount++);
		    if (currentlyDisplayed == null)
			continue;
		    stats.addDisplayTime(currentlyDisplayed.getName(), name,
			    (int) seconds, (int) (pricing * seconds));
		    sendStatistics();
		    displayPushes();
		}
	    }
	});;
    private LinkedList<Commercial> push = new LinkedList<>();
    private LinkedList<Commercial> pushed = new LinkedList<>();
    private ArrayList<Commercial> commercials = new ArrayList<Commercial>();
    private ArrayList<Commercial> commercialsLoop = new ArrayList<Commercial>();
    private Statistics stats = new Statistics();

    private boolean active = false;
    private Commercial currentlyDisplayed = null;

    // BEHAVIOR
    // C'tors
    public Sign() {
	this.name = "";
	this.pricing = 0;
	this.areaName = "";
	this.areaType = AreaType.Non_Urban;
	this.CanShowImage = false;
    }

    public Sign(Sign s) {
	this(s.name, s.pricing, s.areaName, s.areaType, s.CanShowImage);
	ArrayList<Commercial> comms = new ArrayList<>();
	comms.addAll(s.getCommercials());
	this.commercials = comms;
	this.stats = new Statistics(s.getStats());
    }

    public Sign(String val1, String val2, AreaType valueOf, boolean boolean1) {
	this(val1, 0, val2, valueOf, boolean1);
    }

    public Sign(String name2, double pricing2, String areaName2,
	    AreaType areaType2, boolean canImage) {
	this.name = name2;
	this.pricing = pricing2;
	this.areaName = areaName2;
	this.areaType = areaType2;
	this.CanShowImage = canImage;
    }

    public void addListener(ISignDataListener listener) {
	listeners.add(listener);
    }

    public void removeListener(ISignDataListener listener) {
	listeners.remove(listener);
    }

    private void displayPushes() {

	Date d1;
	Date d2;
	long seconds;
	while (active && push.size() > 0) {
	    currentlyDisplayed = push.get(0);
	    for (ISignDataListener l : listeners)
		l.onCommercialDiaplyed(currentlyDisplayed, Sign.this);
	    d1 = Calendar.getInstance().getTime();
	    seconds = _COMERCIAL_DURATION_SEC;
	    do {
		try {
		    Thread.sleep(seconds * 1000);

		} catch (InterruptedException e) {
		    e.printStackTrace();
		}
		d2 = Calendar.getInstance().getTime();
		seconds = _COMERCIAL_DURATION_SEC
			- ((d2.getTime() - d1.getTime()) / 1000);
	    } while (seconds > 0);

	    stats.addDisplayTime(currentlyDisplayed.getName(), name,
		    _COMERCIAL_DURATION_SEC, 0);
	    push.remove(0);
	}
    }


    public void startSignClock() {
	signClock = new Thread(new Runnable() {
	    @Override
	    public void run() {
		int loopCount = 0;
		while (active) {
		    Date d1 = Calendar.getInstance().getTime();
		    try {
			runCycle();
			Thread.sleep(_COMERCIAL_DURATION_SEC * 1000);
		    } catch (InterruptedException e) {
			e.printStackTrace();
		    }
		    Date d2 = Calendar.getInstance().getTime();
		    long seconds = (d2.getTime() - d1.getTime()) / 1000;
		    // System.out.println(loopCount++);
		    if (currentlyDisplayed == null)
			continue;
		    stats.addDisplayTime(currentlyDisplayed.getName(), name,
			    (int) seconds, (int) (pricing * seconds));
		    sendStatistics();
		    displayPushes();
		}
	    }
	});
	active = true;
	signClock.start();
    }

    public Statistics getStats() {
	return stats;
    }

    // -----------------------------------------------
    public void runCycle() {
	if (commercials.size() == 0)
	    return;
	if (commercialsLoop.size() == 0)
	    commercialsLoop.addAll(commercials);
	currentlyDisplayed = commercialsLoop.get(0);
	notifyCommercialChange(currentlyDisplayed);
	commercialsLoop.remove(0);

    }

    public void notifyCommercialChange(Commercial c) {
	for (ISignDataListener l : listeners)
	    l.onCommercialDiaplyed(c, this);
    }

    public void sendStatistics() {
	for (ISignDataListener l : listeners)
	    l.onStatsUpdate(stats, this);
    }

    public void setCommercials(Set<Commercial> commercials) {
	this.commercials.clear();
	for (Commercial i : commercials) {
	    this.commercials.add(i);
	}
    }

    public void addCommercialToRunList(Commercial commercial) {
	this.commercials.add(commercial);
    }

    public void cleanRunList() {
	this.commercials.clear();
	this.commercialsLoop.clear();
    }

    public void setSignPricing(double pricing) {
	this.pricing = pricing;
    }

    public boolean signcanIamge() {
	return this.CanShowImage;
    }

    public void pushToRuninglist(Commercial commercial) {
//	stopSignClock();
	this.push.add(commercial);
	signClock.interrupt();

//	startSignClock();
    }

    public String getSignName() {
	return name;
    }

    public String getAreaName() {
	return areaName;
    }

    public AreaType getAreaType() {
	return areaType;
    }

    public double getPricing() {
	return pricing;
    }

    public ArrayList<Commercial> getCommercials() {
	return commercials;
    }

    @Override
    public String toString() {
	return new StringBuffer(" Sign Name : ").append(this.name)
		.append(" Area Name : ").append(this.areaName)
		.append(" Area Type : ").append(this.areaType).toString();
    }

    @Override
    public int hashCode() {
	return (name + areaName + areaType).hashCode();
    }

    @Override
    public boolean equals(Object obj) {
	Sign o = (Sign) obj;
	return (name.equals(o.name) && areaName.equals(o.areaName)
		&& areaType.equals(o.areaType));
    }

    public void stopSignClock() {
	active = false;
	signClock.interrupt();
    }

    public void setAreaName(String areaName2) {
	this.areaName = areaName2;

    }

    public void setAreaType(AreaType areaType2) {
	this.areaType = areaType2;
    }

    public boolean getCanImage() {
	return CanShowImage;
    }

    public void setCanImage(boolean canImage) {
	this.CanShowImage = canImage;

    }

}