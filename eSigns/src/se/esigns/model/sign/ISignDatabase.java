package se.esigns.model.sign;

import se.esigns.model.commercial.Commercial;
import se.esigns.model.publisher.Publisher;

public interface ISignDatabase {
    
    public void updatePublisherStatistics(Publisher publisher,Commercial commercial,Sign displayedIn, int secondsDisplayed,
	     int chargedAmount);
    
    
}
