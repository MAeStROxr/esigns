package se.esigns.model.sign;

public interface ISignsDatabaseListener {
    public void onSignAdded(Sign sign);
    public void onSignRemoved(Sign sign);
    public void onSignEdited(Sign sign);
}
