package se.esigns.model.sign;

import java.util.Map;

import se.esigns.model.commercial.Commercial;

public interface ISignAllocatorDatabase extends ISignsDatabase {
    public void clearCommercialsFromSign(String signName);
    public boolean addCommercialToSign(Commercial c,String signName );
    public void moveCommercialToExpired(Commercial c);
    public void updatePublisherStatistics(String publisher,String commercial,String displayedIn, int secondsDisplayed,
	     int chargedAmount);
    public Map<String, Commercial> getCommercials();
}
