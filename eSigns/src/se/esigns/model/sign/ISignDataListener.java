package se.esigns.model.sign;

import se.esigns.model.commercial.Commercial;
import se.esigns.model.publisher.Statistics;

public interface ISignDataListener {
    public void onCommercialDiaplyed(Commercial commercial, Sign sign);
    public void onStatsUpdate(Statistics stats,Sign sign);
}
