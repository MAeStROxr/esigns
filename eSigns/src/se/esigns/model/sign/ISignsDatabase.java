package se.esigns.model.sign;

import java.util.Map;
import java.util.Set;

import se.esigns.controller.publisher.PublisherController;
import se.esigns.model.commercial.Commercial;

public interface ISignsDatabase {

    

    public Map<String, Sign> getSigns();

    public Set<String> getAreas();
    
    public void registerSignListener(ISignDataListener signView, String signName);

    public void unregisterSignListener(ISignDataListener signView, String signName);

    public void registerSignsListener(ISignsDatabaseListener signsView);

    public void unregisterSignsListener(ISignsDatabaseListener signsView);

   
}