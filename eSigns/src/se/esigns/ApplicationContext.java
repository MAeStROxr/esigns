package se.esigns;

public class ApplicationContext {
    private static  SignsApplication eSignsApp;
    public static void createContext(){
	eSignsApp=new SignsApplication();
    }
    public static SignsApplication getApp(){
	return eSignsApp;
    }
}
