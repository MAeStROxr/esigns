package se.esigns.view.management;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Device;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import se.esigns.controller.management.ManagerController;

public class OficialPublisherSignUp {
	private ManagerController mc;
    private ManagerView managerView;
    final Display display;
    final Shell shell;
    
    public OficialPublisherSignUp(Display display, ManagerView managerView){
    	shell = new Shell(display, SWT.DIALOG_TRIM | SWT.CENTER);
		this.display = display;
		this.managerView = managerView;
	
		this.mc = managerView.getController();
    }
    
    public void initUI() {
    	Device device = Display.getCurrent();
    	Color background_color = new Color(device, 200, 200, 220);
	
		shell.setLayout(new FormLayout());
	
		shell.setBackground(background_color);
		// MenegerMainWorkPage widget definitions
		Label header_lable = new Label(shell, SWT.LEFT);
        Font header_lable_font = new Font(header_lable.getDisplay(), new FontData("Arial", 12, SWT.BOLD));
        header_lable.setFont(header_lable_font);
        header_lable.setText("Sign Up");
        
        Label explenation_lable = new Label(shell, SWT.LEFT);
        explenation_lable.setText("Enter Company name to register to the system.");
        
        Label signup_username_lable = new Label(shell, SWT.LEFT);
        signup_username_lable.setText("Username:");
        
        Label signup_password_lable = new Label(shell, SWT.LEFT);
        signup_password_lable.setText("Password:");
        
        
        final Text signup_username_text = new Text(shell, SWT.SINGLE);
        signup_username_text.setTextLimit(200);
        signup_username_text.setToolTipText("Publisher Company name must be up to 200 characters");
        
        final Text signup_password_text = new Text(shell, SWT.SINGLE);
        signup_password_text.setTextLimit(200);
        signup_password_text.setToolTipText("Publisher password name must be up to 200 characters");
        
        
        Button ok_button = new Button(shell, SWT.PUSH);
        Font login_button_font = new Font(ok_button.getDisplay(), new FontData("Arial", 10, SWT.BOLD));
        ok_button.setFont(login_button_font);
        ok_button.setText("OK");
        
        
        Button cancel_button = new Button(shell, SWT.PUSH);
        Font signup_button_font = new Font(cancel_button.getDisplay(), new FontData("Arial", 10, SWT.BOLD));
        cancel_button.setFont(signup_button_font);
        cancel_button.setText("Cancel");
        
		// MenegerMainWorkPage Handle Listeners
        ok_button.addListener(SWT.Selection, new Listener() {
        	public void handleEvent(Event e) {
        		String username = signup_username_text.getText();
        		String password = signup_password_text.getText();
        		mc.addOfficial(username,password);
        		
        		shell.dispose();
                }
        });
        
        cancel_button.addListener(SWT.Selection, new Listener() {
        	public void handleEvent(Event e) {
        	    shell.dispose();
                }
        });
		// MenegerMainWorkPage Formating
        FormData header_label_data = new FormData();
        header_label_data.top = new FormAttachment(0, 10);
        header_label_data.left = new FormAttachment(40, 0);
        header_lable.setLayoutData(header_label_data);
        
        FormData explenation_label_data = new FormData();
        explenation_label_data.top = new FormAttachment(header_lable, 10);
        explenation_label_data.left = new FormAttachment(0, 10);
        explenation_label_data.right = new FormAttachment(100, -40);
        explenation_lable.setLayoutData(explenation_label_data);
        
        FormData signup_username_lable_data = new FormData();
        signup_username_lable_data.top = new FormAttachment(explenation_lable, 10);
        signup_username_lable_data.left = new FormAttachment(0, 10);
        signup_username_lable.setLayoutData(signup_username_lable_data);
        
        FormData signup_password_lable_data = new FormData();
        signup_password_lable_data.top = new FormAttachment(signup_username_lable, 10);
        signup_password_lable_data.left = new FormAttachment(0, 10);
        signup_password_lable.setLayoutData(signup_password_lable_data);
        
        
        FormData signup_username_text_data = new FormData();
        signup_username_text_data.top = new FormAttachment(explenation_lable, 10);
        signup_username_text_data.left = new FormAttachment(signup_username_lable, 15);
        signup_username_text_data.width = 120;
        signup_username_text_data.right = new FormAttachment(100, -20);
        signup_username_text.setLayoutData(signup_username_text_data);
        
        FormData signup_password_text_data = new FormData();
        signup_password_text_data.top = new FormAttachment(signup_username_lable, 10);
        signup_password_text_data.left = new FormAttachment(signup_password_lable, 15);
        signup_password_text_data.width = 120;
        signup_password_text_data.right = new FormAttachment(100, -20);
        signup_password_text.setLayoutData(signup_password_text_data);
        
        FormData ok_button_data = new FormData();
        ok_button_data.top = new FormAttachment(signup_password_lable, 15);
        ok_button_data.left = new FormAttachment(20, 10);
        ok_button.setLayoutData(ok_button_data);
        
        FormData cancel_button_data = new FormData();
        cancel_button_data.top = new FormAttachment(signup_password_lable, 15);
        cancel_button_data.right = new FormAttachment(80, -10);
        cancel_button.setLayoutData(cancel_button_data);
        
		// loginUI run initialization
        shell.setText("Pficial Publisher Signup");
        shell.pack();
        shell.open();
        
        while (!shell.isDisposed()) {
            if (!display.readAndDispatch())
                display.sleep();
        }
    }
    
    private void signUpSecessBox(Shell shell, String company_name) {
        
        int style = SWT.ICON_INFORMATION | SWT.OK;

        MessageBox sucess = new MessageBox(shell, style);
        sucess.setText("Information");
        sucess.setMessage("New Company Sucessfully registered as " + company_name + "."); 
        sucess.open();
    }
    
    private void signUpFailedBox(Shell shell, String company_name,String reason) {
        
        int style = SWT.ICON_INFORMATION | SWT.OK;

        MessageBox failed = new MessageBox(shell, style);
        failed.setText("Information");
        failed.setMessage("Registration for company " + company_name + " failed.\n"+reason); 
        failed.open();
    }
}
