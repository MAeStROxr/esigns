package se.esigns.view.management;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.widgets.Display;

import se.esigns.controller.management.ManagerController;
import se.esigns.controller.management.MarketerController;
import se.esigns.model.management.IManagerDatabaseListener;
import se.esigns.model.sign.Sign;

public class MarketerView {
    private MarketerController mc;
	   // private IManagerDatabase managerData;
	    public String username;
	    public String password;
	    MarketingMainWorkPage main ;
	    Display display;
	    public MarketerView( Display display){
	//	this.managerData=data;
		this.display=display;
		
		
	
}
public void registerController(MarketerController controller){
		mc=controller;
}
public MarketerController getController(){
	return mc;
}

public void onInitUI() {
	main = new MarketingMainWorkPage(display,this);
	
	main.initUI();

}

public void onRefreshSigns() {
	List<Sign> signs=new ArrayList<>();
	for (Sign s :mc.getSigns().values())
	    main.onSignAdded(s);
	
}


}
