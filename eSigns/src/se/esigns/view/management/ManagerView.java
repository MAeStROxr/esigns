package se.esigns.view.management;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.eclipse.swt.widgets.Display;

import se.esigns.controller.management.ManagerController;
import se.esigns.model.management.IManagerDatabase;
import se.esigns.model.management.IManagerDatabaseListener;
import se.esigns.model.publisher.Publisher;
import se.esigns.model.publisher.PublishingPrivilege;
import se.esigns.model.sign.AreaType;
import se.esigns.model.sign.Sign;

public  class ManagerView  {
    private ManagerController mc;
	   // private IManagerDatabase managerData;
	    private List<IManagerDatabaseListener> dataListeners=new ArrayList<>();
	    public String username;
	    public String password;
	    ManagerMainWorkPage main ;
	    Display display;
	    public ManagerView( Display display){
	//	this.managerData=data;
		this.display=display;
		
		dataListeners.add(main);
	
    }
    public void registerController(ManagerController controller){
		mc=controller;
    }
    public ManagerController getController(){
	return mc;
    }
   
    public void onInitUI() {
	main = new ManagerMainWorkPage(display,this);
	
	main.initUI();

    }
    
    public void onRefreshSigns() {
	// TODO Auto-generated method stub
	List<Sign> signs=new ArrayList<>();
	for (Sign s :mc.getSigns().values())
	    main.onSignAdded(s);
	
    }
    
    public void onRefreshPublishers(){
	main.refreshPublishers(mc.getPublishers());
    }

}
