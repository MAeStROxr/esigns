package se.esigns.view.management;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.text.View;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Device;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

import se.esigns.ApplicationContext;
import se.esigns.controller.management.ManagerController;
import se.esigns.model.management.IManagerDatabaseListener;
import se.esigns.model.publisher.OfficialPublisher;
import se.esigns.model.publisher.Publisher;
import se.esigns.model.publisher.PType;
import se.esigns.model.publisher.PublishingPrivilege;
import se.esigns.model.sign.AreaType;
import se.esigns.model.sign.Sign;

public class ManagerMainWorkPage implements IManagerDatabaseListener {

	private static int SIGN_COLLUM_NUMBER = 8;
	private static int PUBLISHER_COLLUM_NUMBER = 3;

	private ManagerController mc;
	private ManagerView managerView;

	/**
	 * UI DATA
	 * 
	 * 
	 * @param display
	 * @param managerView
	 */
	Device device;

	Color background_color;
	final Shell shell;

	String[][] sign_list = {};

	Label title_label;
	Font header_lable_font;

	Composite sign_command_buttons;
	RowLayout Buttons_row = new RowLayout(SWT.HORIZONTAL);

	Button sign_editor_button;
	Button user_editor_button;
	final Composite work_area;

	Composite sign_editor;

	String[] titles;
	Table sign_table;
	Label table_name_label;
	Label location_edit_label;
	Label name_edit_label;
	Text name_edit_text;
	final Display display;
	String publisher_name;

	Text location_edit_text;
	Label area_edit_label;

	Combo area_edit_combo;

	Label Type_edit_label;

	Combo type_edit_combo;

	Button add_button;
	Button edit_button;
	Button remove_button;

	public ManagerMainWorkPage(Display display, ManagerView managerView) {
		shell = new Shell(display, SWT.SHELL_TRIM | SWT.CENTER);
		work_area = new Composite(shell, SWT.CENTER);
		this.display = display;
		this.managerView = managerView;

		this.mc = managerView.getController();
	}

	public void initUI() {

		device = Display.getCurrent();
		background_color = new Color(device, 200, 200, 220);

		shell.setLayout(new FormLayout());

		shell.setBackground(background_color);

		sign_list = new String[0][];

		// MenegerMainWorkPage widget definitions

		Label title_label = new Label(shell, SWT.CENTER);
		Font header_lable_font = new Font(title_label.getDisplay(),
				new FontData("Arial", 12, SWT.BOLD));
		title_label.setFont(header_lable_font);
		title_label.setText("Sign Menager Site");
		title_label.setBackground(background_color);

		sign_command_buttons = new Composite(shell, SWT.CENTER);
		sign_command_buttons.setLayout(Buttons_row);
		sign_command_buttons.setBackground(background_color);

		sign_editor_button = new Button(sign_command_buttons, SWT.PUSH);
		sign_editor_button.setText("Add or edit Signs");

		user_editor_button = new Button(sign_command_buttons, SWT.PUSH);
		user_editor_button.setText("Edit user privilages");

		work_area.setLayout(new FillLayout());
		work_area.setBackground(background_color);

		// MenegerMainWorkPage Handle Listeners

		// sign editor button press listener
		sign_editor_button.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event e) {
				Control[] children = work_area.getChildren();
				for (int i = 0; i < children.length; i++) {
					children[i].dispose();
				}
				InitSignEditor(shell, work_area, background_color);
				work_area.layout();
				shell.layout();
				shell.pack();
			}
		});

		// user editor press listener
		user_editor_button.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event e) {
				Control[] children = work_area.getChildren();
				for (int i = 0; i < children.length; i++) {
					children[i].dispose();
				}
				InitClearanceEditor(work_area, background_color);
				shell.pack();
			}
		});

		// MenegerMainWorkPage Formating

		FormData title_label_data = new FormData();
		title_label_data.top = new FormAttachment(0, 10);
		title_label_data.left = new FormAttachment(30, 0);
		title_label_data.right = new FormAttachment(80, 0);
		title_label.setLayoutData(title_label_data);
		FormData sign_command_buttons_data = new FormData();
		sign_command_buttons_data.top = new FormAttachment(title_label, 15);
		sign_command_buttons_data.left = new FormAttachment(20, 0);
		sign_command_buttons_data.right = new FormAttachment(90, 0);
		sign_command_buttons.setLayoutData(sign_command_buttons_data);
		FormData work_area_data = new FormData();
		work_area_data.top = new FormAttachment(sign_command_buttons, 15);
		work_area_data.left = new FormAttachment(5, 0);
		work_area_data.right = new FormAttachment(95, 0);
		work_area_data.bottom = new FormAttachment(95, 0);
		work_area.setLayoutData(work_area_data);

		// loginUI run initialization

		shell.setText("Menager Site");
		shell.pack();
		shell.open();
		mc.registerManagerListener(this);
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
		mc.unregisterManagerListener(this);

	}

	// ::::::::::::::::::::::::::
	// component functions
	// ::::::::::::::::::::::::::
	Button official_button;

	private void InitSignEditor(final Shell shell, Composite work_area,
			Color background_color) {

		// InitSignEditor widget definitions

		sign_editor = new Composite(work_area, SWT.CENTER);
		sign_editor.setLayout(new GridLayout(SIGN_COLLUM_NUMBER, false));
		sign_editor.setBackground(background_color);

		table_name_label = new Label(sign_editor, SWT.CENTER);
		table_name_label.setText("Sign Table");
		table_name_label.setBackground(background_color);

		sign_table = new Table(sign_editor, SWT.BORDER);
		sign_table.setHeaderVisible(true);
		sign_table.setLinesVisible(true);

		String[] titles = { "Name", "Location", "Area Type", "Type" };

		for (int i = 0; i < titles.length; i++) {
			TableColumn column = new TableColumn(sign_table, SWT.NULL);
			column.setText(titles[i]);
			column.setWidth(130);
		}

		managerView.onRefreshSigns();
		name_edit_label = new Label(sign_editor, SWT.LEFT);
		name_edit_label.setText("Name:");
		name_edit_label.setBackground(background_color);

		name_edit_text = new Text(sign_editor, SWT.SINGLE);

		location_edit_label = new Label(sign_editor, SWT.LEFT);
		location_edit_label.setText("Location:");
		location_edit_label.setBackground(background_color);

		location_edit_text = new Text(sign_editor, SWT.SINGLE);

		area_edit_label = new Label(sign_editor, SWT.LEFT);
		area_edit_label.setText("area type:");
		area_edit_label.setBackground(background_color);

		area_edit_combo = new Combo(sign_editor, SWT.DROP_DOWN | SWT.READ_ONLY);
		for (AreaType areaType : AreaType.values())
			area_edit_combo.add(areaType.toString());

		Type_edit_label = new Label(sign_editor, SWT.LEFT);
		Type_edit_label.setText("Type:");

		type_edit_combo = new Combo(sign_editor, SWT.DROP_DOWN | SWT.READ_ONLY);
		type_edit_combo.add("Text only");
		type_edit_combo.add("Image or text");

		add_button = new Button(sign_editor, SWT.PUSH);
		add_button.setText("add");

		edit_button = new Button(sign_editor, SWT.PUSH);
		edit_button.setText("edit");

		remove_button = new Button(sign_editor, SWT.PUSH);
		remove_button.setText("remove");

		// InitSignEditor Handle Listeners

		// table selection listener
		sign_table.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event e) {
				final TableItem[] selected_sign = sign_table.getSelection();
				name_edit_text.setText(selected_sign[0].getText(0));
				location_edit_text.setText(selected_sign[0].getText(1));
				area_edit_combo.select(area_edit_combo.indexOf(selected_sign[0]
						.getText(2)));
				type_edit_combo.select(type_edit_combo.indexOf(selected_sign[0]
						.getText(3)));
			}
		});

		// add new sign button press listener
		add_button.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event e) {
				callAddSign(shell, sign_table, name_edit_text,
						location_edit_text, area_edit_combo, type_edit_combo);
			}
		});

		// edit sign button press listener
		edit_button.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event e) {
				edditSign(shell, sign_table, name_edit_text,
						location_edit_text, area_edit_combo, type_edit_combo);
			}
		});

		// remove sign button press listener
		remove_button.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event e) {
				removeSign(sign_table);
			}
		});

		// InitSignEditor Formating

		GridData single_cell_data = new GridData(SWT.FILL, 0, true, true);
		GridData full_line_data = new GridData(SWT.FILL, 0, true, true);
		full_line_data.horizontalSpan = SIGN_COLLUM_NUMBER;
		GridData table_data = new GridData(SWT.FILL, 0, true, true);
		table_data.minimumHeight = 400;
		table_data.horizontalSpan = SIGN_COLLUM_NUMBER;

		table_name_label.setLayoutData(full_line_data);
		sign_table.setLayoutData(table_data);
		name_edit_label.setLayoutData(single_cell_data);
		name_edit_text.setLayoutData(single_cell_data);
		location_edit_label.setLayoutData(single_cell_data);
		location_edit_text.setLayoutData(single_cell_data);
		area_edit_label.setLayoutData(single_cell_data);
		Type_edit_label.setLayoutData(single_cell_data);
		type_edit_combo.setLayoutData(single_cell_data);
		add_button.setLayoutData(single_cell_data);
		edit_button.setLayoutData(single_cell_data);
		remove_button.setLayoutData(single_cell_data);

		// mc.addSign("sign1", "location1", AreaType.Non_Urban, false);
		// mc.addSign("sign2", "location2", AreaType.Non_Urban, true);
		// mc.addSign("sign3", "location2", AreaType.Urban, false);

	}

	Table publisher_table;

	private void InitClearanceEditor(Composite work_area, Color background_color) {

		// InitClearanceEditor widget definitions

		Composite sign_editor = new Composite(work_area, SWT.CENTER);
		sign_editor.setLayout(new GridLayout(PUBLISHER_COLLUM_NUMBER, false));
		sign_editor.setBackground(background_color);

		Label table_name_label = new Label(sign_editor, SWT.CENTER);
		table_name_label.setText("Publisher List");
		table_name_label.setBackground(background_color);

		publisher_table = new Table(sign_editor, SWT.BORDER);
		publisher_table.setHeaderVisible(true);
		publisher_table.setLinesVisible(true);

		String[] titles = { "Publisher Name", "Official" };

		for (int i = 0; i < titles.length; i++) {
			TableColumn column = new TableColumn(publisher_table, SWT.NULL);
			column.setText(titles[i]);
			column.setWidth(130);
		}

		final Button new_publisher_button = new Button(sign_editor, SWT.PUSH);
		new_publisher_button.setText("New Publisher");
		new_publisher_button.setBackground(background_color);

		final Button remove_button = new Button(sign_editor, SWT.PUSH);
		remove_button.setText("remove");
		remove_button.setEnabled(false);

		final Button edit_button = new Button(sign_editor, SWT.PUSH);
		edit_button.setText("edit");
		edit_button.setEnabled(false);

		official_button = new Button(sign_editor, SWT.CHECK);
		official_button.setText("official");
		official_button.setEnabled(false);

		// InitClearanceEditor Handle Listeners

		// table selection listener
		publisher_table.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event e) {
				final TableItem[] publisher = publisher_table.getSelection();
				remove_button.setEnabled(true);
				edit_button.setEnabled(true);
				official_button.setEnabled(true);
				String officialStatus = publisher[0].getText(1);
				publisher_name = publisher[0].getText(0);
				official_button.setSelection(officialStatus.equals("Yes"));
			}
		});

		// add publisher button press listener
		new_publisher_button.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event e) {
				OficialPublisherSignUp signup = new OficialPublisherSignUp(
						display, managerView);
				signup.initUI();
			}
		});

		// edit publisher clearance button press listener
		edit_button.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event e) {
				edditPublisher(publisher_table, display);
			}
		});

		// remove publisher button press listener
		remove_button.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event e) {
				final TableItem[] publisher = publisher_table.getSelection();
				String username = publisher[0].getText();
				mc.removePublisher(username);
			}
		});

		official_button.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event e) {
				final TableItem[] publisher = publisher_table.getSelection();
				String username = publisher[0].getText(0);
				if (official_button.getSelection())
					mc.promoteToOfficial(username);
				else
					mc.demoteOfficial(username);

			}
		});

		// InitClearanceEditor Formating

		GridData single_cell_data = new GridData(SWT.FILL, 0, true, true);
		GridData full_line_data = new GridData(SWT.FILL, 0, true, true);
		full_line_data.horizontalSpan = PUBLISHER_COLLUM_NUMBER;
		GridData table_data = new GridData(SWT.FILL, 0, true, true);
		table_data.minimumHeight = 400;
		table_data.horizontalSpan = PUBLISHER_COLLUM_NUMBER;
		GridData new_publisher_button_data = new GridData(SWT.FILL, 0, true,
				true);
		new_publisher_button_data.horizontalSpan = PUBLISHER_COLLUM_NUMBER - 1;

		table_name_label.setLayoutData(full_line_data);
		publisher_table.setLayoutData(table_data);
		new_publisher_button.setLayoutData(new_publisher_button_data);
		remove_button.setLayoutData(single_cell_data);
		edit_button.setLayoutData(single_cell_data);

		managerView.onRefreshPublishers();
	}

	HashSet<String> pulisher_allowed_areas = new HashSet<String>();
	HashSet<AreaType> pulisher_allowed_area_types = new HashSet<AreaType>();

	public void initSignPrivilageList(Display display) {
		Device device = Display.getCurrent();
		final Color background_color = new Color(device, 200, 200, 220);

		final Shell shell = new Shell(display, SWT.SHELL_TRIM | SWT.CENTER);

		shell.setLayout(new FormLayout());

		shell.setBackground(background_color);

		final String Current_pulisher = publisher_name;

		AreaType[] area_types = AreaType.values();

		final ArrayList<String> area_types_names = new ArrayList<String>();

		for (AreaType type : area_types) {
			area_types_names.add(type.toString());
		}

		final Set<String> location_set = mc.getLocations();

		for (Publisher publisher : mc.getPublishers()) {
			if (publisher.getUsername().matches(Current_pulisher)) {
				if (publisher.isOfficial()) {
					pulisher_allowed_areas = ((OfficialPublisher) publisher)
							.getPrivilege().areaNames;
					pulisher_allowed_area_types = ((OfficialPublisher) publisher)
							.getPrivilege().areaTypes;
				}
			}
		}

		// initSignPrivilageList widget definitions

		Label title_label = new Label(shell, SWT.CENTER);
		Font header_lable_font = new Font(title_label.getDisplay(),
				new FontData("Arial", 12, SWT.BOLD));
		title_label.setFont(header_lable_font);
		title_label.setText("Sign Menager Site");
		title_label.setBackground(background_color);

		final Table location_table = new Table(shell, SWT.BORDER | SWT.MULTI);
		location_table.setHeaderVisible(true);
		location_table.setLinesVisible(true);

		String[] titles = { "Location", "clearance" };

		for (int i = 0; i < titles.length; i++) {
			TableColumn column = new TableColumn(location_table, SWT.NULL);
			column.setText(titles[i]);
			column.setWidth(130);
		}

		refreshLocationTable(location_table, location_set);

		final Table Area_type_table = new Table(shell, SWT.BORDER | SWT.MULTI);
		location_table.setHeaderVisible(true);
		location_table.setLinesVisible(true);

		String[] titles2 = { "Area Type", "clearance" };

		for (int i = 0; i < titles2.length; i++) {
			TableColumn column = new TableColumn(Area_type_table, SWT.NULL);
			column.setText(titles[i]);
			column.setWidth(130);
		}
		
		refreshAreaTypeTable(Area_type_table, area_types_names);
		
		final Button Location_change_button = new Button(shell, SWT.PUSH);
		Location_change_button.setText("change location clearance");
		Location_change_button.setEnabled(false);

		final Button Area_type_change_button = new Button(shell, SWT.PUSH);
		Area_type_change_button.setText("change area type clearace");
		Area_type_change_button.setEnabled(false);

		Button exit_button = new Button(shell, SWT.PUSH);
		exit_button.setText("exit");

		// initSignPrivilageList Handle Listeners

		// table selection listener
		location_table.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event e) {
				if (pulisher_allowed_areas.contains(location_table
						.getSelection()[0].getText(0))) {
					Location_change_button.setText("remove location clearance");
				} else {
					Location_change_button.setText("add location clearance");
				}
				Location_change_button.setEnabled(true);
			}
		});

		Area_type_table.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event e) {
				if (pulisher_allowed_area_types.contains(AreaType
						.valueOf(Area_type_table.getSelection()[0].getText(0)))) {
					Area_type_change_button
							.setText("remove area type clearance");
				} else {

					Area_type_change_button.setText("add area type clearance");
				}
				Area_type_change_button.setEnabled(true);
			}
		});

		// change button press listener
		Location_change_button.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event e) {
				if (pulisher_allowed_areas.contains(location_table
						.getSelection()[0].getText(0))) {
					mc.addLocationPrivilege(
							location_table.getSelection()[0].getText(0),
							Current_pulisher);
				} else {
					mc.removeLocationPrivilege(
							location_table.getSelection()[0].getText(0),
							Current_pulisher);
				}
				refreshLocationTable(location_table, location_set);
			}
		});

		// change button press listener
		Area_type_change_button.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event e) {
				if (pulisher_allowed_area_types.contains(AreaType
						.valueOf(Area_type_table.getSelection()[0].getText(0)))) {
					mc.addAreaTypePrivilege(AreaType.valueOf(Area_type_table
							.getSelection()[0].getText(0)), Current_pulisher);
				} else {
					mc.removeAreaTypePrivilege(AreaType.valueOf(Area_type_table
							.getSelection()[0].getText(0)), Current_pulisher);
				}
				refreshAreaTypeTable(Area_type_table, area_types_names);
			}
		});

		// exit button press listener
		exit_button.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event e) {
				shell.dispose();
			}
		});

		// initSignPrivilageList Formating

		FormData title_label_data = new FormData();
		title_label_data.top = new FormAttachment(0, 5);
		title_label_data.left = new FormAttachment(40, 0);
		title_label.setLayoutData(title_label_data);

		FormData location_table_data = new FormData();
		location_table_data.top = new FormAttachment(title_label, 10);
		location_table_data.left = new FormAttachment(5, 0);
		location_table.setLayoutData(location_table_data);

		FormData Area_type_table_data = new FormData();
		Area_type_table_data.top = new FormAttachment(title_label, 10);
		Area_type_table_data.left = new FormAttachment(location_table, 10);
		Area_type_table.setLayoutData(Area_type_table_data);

		FormData Location_change_button_data = new FormData();
		Location_change_button_data.top = new FormAttachment(location_table, 20);
		Location_change_button_data.left = new FormAttachment(5, 0);
		Location_change_button.setLayoutData(Location_change_button_data);

		FormData Area_type_change_button_data = new FormData();
		Area_type_change_button_data.top = new FormAttachment(location_table,
				20);
		Area_type_change_button_data.left = new FormAttachment(location_table,
				10);
		Area_type_change_button.setLayoutData(Area_type_change_button_data);

		FormData exit_button_data = new FormData();
		exit_button_data.top = new FormAttachment(location_table, 20);
		exit_button_data.right = new FormAttachment(95, -20);
		exit_button.setLayoutData(exit_button_data);

		// loginUI run initialization

		shell.setText("Menager Site");
		// shell.pack();
		shell.open();

		while (!shell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
	}

	// ::::::::::::::::::::::::::
	// Utility functions
	// ::::::::::::::::::::::::::
	public void refreshPublishers(List<Publisher> publishers) {
		publisher_table.removeAll();
		for (Publisher p : publishers) {
			addPublisher(p.getUsername(),
					PType.Any.getType(p) == PType.OFFICIAL);
		}
	}

	private void addPublisher(String username, boolean official) {
		if (publisher_table == null)
			return;
		TableItem publisher = new TableItem(publisher_table, SWT.NULL);
		publisher.setText(0, username);
		publisher.setText(1, official ? "Yes" : "No");

	}

	// private void removePublisher(String username, boolean official){
	// TableItem publisher = new TableItem(publisher_table, SWT.NULL);
	// publisher.setText(0, username);
	// publisher.setText(1, official ? "Yes" : "No");
	//
	// }*-

	@Override
	public void onSignAdded(Sign sign) {
		if (sign_table != null && !sign_table.isDisposed())
			addSign(sign.getSignName(), sign.getAreaName(), sign.getAreaType(),
					sign.signcanIamge(), sign_table);

	}

	public void addSign(String name, String location, AreaType areaType,
			boolean canImage, Table sign_table) {
		if (sign_table == null || sign_table.isDisposed())
			return;
		TableItem sign = new TableItem(sign_table, SWT.NULL);
		sign.setText(0, name);
		sign.setText(1, location);
		sign.setText(2, areaType.toString());
		sign.setText(3, String.valueOf(canImage));
		// sign_table.pack();

	}

	@Override
	public void onSignRemoved(Sign sign) {
		if (sign_table != null && !sign_table.isDisposed())
			removeSign(sign, sign_table);
	}

	private void removeSign(Sign sign, Table sign_table) {
		int index = getValueIndex(sign_table, 0, sign.getSignName());
		if (index > -1)
			sign_table.remove(index);
	}

	@Override
	public void onSignEdited(Sign sign) {
		if (sign_table != null && !sign_table.isDisposed())
			editSign(sign, sign_table);

	}

	private void editSign(Sign sign, Table sign_table) {
		int index = getValueIndex(sign_table, 0, sign.getSignName());
		TableItem item = sign_table.getItem(index);
		item.setText(1, sign.getAreaName());
		item.setText(2, sign.getAreaType().toString());
		item.setText(3, sign.getCanImage() ? "Image or text" : "Text only");
		item.setText(4, Double.toString(sign.getPricing()));
	}

	public void refreshSigns(ArrayList<Sign> signs, Table sign_table) {
		sign_table.removeAll();
		for (Sign s : signs) {
			addSign(s.getSignName(), s.getAreaName(), s.getAreaType(), true,
					sign_table);

		}
	}

	public void refreshSigns(List<Sign> signs) {
		sign_table.removeAll();
		for (Sign s : signs) {
			addSign(s.getSignName(), s.getAreaName(), s.getAreaType(), true,
					s.getPricing());
		}
	}

	public void addSign(String name, String location, AreaType areaType,
			boolean canImage, double pricing) {
		if (sign_table == null)
			return;
		TableItem sign = new TableItem(sign_table, SWT.NULL);
		sign.setText(0, name);
		sign.setText(1, location);
		sign.setText(2, areaType.toString());
		sign.setText(3, canImage ? "Image or text" : "Text only");
		sign.setText(4, Double.toString(pricing));
	}

	private int getValueIndex(Table table, int column, String value) {
		if (table == null || table.isDisposed())
			return -1;
		TableItem[] ti = table.getItems();
		int index = -1;
		for (TableItem t : ti) {
			String tableValue = t.getText(column);
			if (tableValue.equals(value))
				index = table.indexOf(t);
		}
		return index;
	}

	public void addSign(String name, String location, AreaType areaType,
			boolean canImage) {
		if (sign_table == null)
			return;
		TableItem sign = new TableItem(sign_table, SWT.NULL);
		sign.setText(0, name);
		sign.setText(1, location);
		sign.setText(2, areaType.toString());
		sign.setText(3, canImage ? "Image or text" : "Text only");
	}

	private Sign getSelectedSign() {
		TableItem[] s = sign_table.getSelection();
		return new Sign(s[0].getText(0), s[0].getText(1), AreaType.valueOf(s[0]
				.getText(2)), Boolean.getBoolean(s[0].getText(3)));
	}

	private void callAddSign(Shell shell, Table sign_table, Text name,
			Text location, Combo area, Combo type) {
		String val1 = name.getText();
		String val2 = location.getText();
		String val3 = area.getText();
		String val4 = type.getText();
		if (val1.isEmpty() || val2.isEmpty() || val3.isEmpty()
				|| val4.isEmpty()) {
			empty_field_error(shell, val1, val2, val3, val4);
			return;
		}

		mc.addSign(new Sign(val1, val2, AreaType.valueOf(val3), Boolean
				.getBoolean(val4)));

	}

	private void edditSign(Shell shell, Table sign_table, Text name,
			Text location, Combo area, Combo type) {
		String val1 = name.getText();
		String val2 = location.getText();
		String val3 = area.getText();
		String val4 = type.getText();
		TableItem[] selected_sign = sign_table.getSelection();
		// if (selected_sign.length == 0) {
		// unselected_error(shell, "Sign");
		// return;
		// }
		if (val1.isEmpty() || val2.isEmpty() || val3.isEmpty()
				|| val4.isEmpty()) {
			empty_field_error(shell, val1, val2, val3, val4);
			return;
		}
		Sign s = new Sign(val1, val2, AreaType.valueOf(val3),
				val4.equals("Image or text") ? true : false);
		mc.editSign(s);

	}

	private void removeSign(Table sign_table) {
		int selected_index = sign_table.getSelectionIndex();
		if (selected_index < 0) {
			return;
		}

		mc.removeSign(getSelectedSign());

	}

	private void edditPublisher(Table sign_table, Display display) {
		initSignPrivilageList(display);
		return;
	}

	void empty_field_error(Shell shell, String val1, String val2, String val3,
			String val4) {
		String messege = "The following fields have not been filled:\n";
		if (val1.isEmpty()) {
			messege += "\n\tName";
		}
		if (val2.isEmpty()) {
			messege += "\n\tLocation";
		}
		if (val3.isEmpty()) {
			messege += "\n\tArea";
		}
		if (val4.isEmpty()) {
			messege += "\n\tType";
		}

		int style = SWT.ICON_ERROR | SWT.OK;

		MessageBox dia = new MessageBox(shell, style);
		dia.setText("Error");
		dia.setMessage(messege);
		dia.open();
	}

	void unselected_error(Shell shell, String object) {
		String messege = object + " not chosen";

		int style = SWT.ICON_ERROR | SWT.OK;

		MessageBox dia = new MessageBox(shell, style);
		dia.setText("Error");
		dia.setMessage(messege);
		dia.open();
	}
	
	void refreshLocationTable(Table location_table, Set<String> location_set){
		location_table.removeAll();
		for (String location : location_set) {
			TableItem item = new TableItem(location_table, SWT.NULL);
			item.setText(0, location);
			if (pulisher_allowed_areas.contains(location)) {
				item.setText(1, "yes");
			} else {
				item.setText(1, "no");
			}
		}
	}
	
	void refreshAreaTypeTable(Table Area_type_table, ArrayList<String> area_types_names){
		for (String at : area_types_names) {
			Area_type_table.removeAll();
			TableItem item = new TableItem(Area_type_table, SWT.NULL);
			item.setText(0, at);
			if (pulisher_allowed_area_types.contains(AreaType.valueOf(at))) {
				item.setText(1, "yes");
			} else {
				item.setText(1, "no");
			}
		}
	}

	@Override
	public void onPublisherAdded(Publisher publisher) {
		if (publisher_table == null || publisher_table.isDisposed())
			return;
		addPublisher(publisher.getUsername(),
				PType.Any.getType(publisher) == PType.OFFICIAL);
	}

	@Override
	public void onPublisherPrivilegeEdited(PublishingPrivilege pp,
			Publisher publisher) {

	}

	@Override
	public void onPublisherRemoved(Publisher publisher) {
		int index = getValueIndex(publisher_table, 0, publisher.getUsername());
		if (index > -1)
			publisher_table.remove(index);
	}

	@Override
	public void onPublisherEdited(Publisher p) {
		int index = getValueIndex(publisher_table, 0, p.getUsername());
		if (index < 0)
			return;
		TableItem item = publisher_table.getItem(index);
		boolean isOfficial = p.isOfficial();
		item.setText(1, isOfficial ? "Yes" : "No");
		// official_button.setSelection(isOfficial);
	}

	

	@Override
	public void onLocationPrivilegeAdded(String location, String username) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onLocationPrivilegeRemoved(String location, String username) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onAreaTypePrivilegeAdded(AreaType areaType, String username) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onAreaTypePrivilegeRemoved(AreaType areaType, String username) {
		// TODO Auto-generated method stub
		
	}
}