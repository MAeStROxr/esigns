package se.esigns.view.management;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Device;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

import se.esigns.controller.management.MarketerController;
import se.esigns.model.sign.AreaType;
import se.esigns.model.sign.ISignsDatabaseListener;
import se.esigns.model.sign.Sign;

public class MarketingMainWorkPage implements ISignsDatabaseListener {
    MarketerView mv;
    MarketerController mc;

    Table sign_table;
    Display display;
    final Color background_color;
    // boolean official;

    public MarketingMainWorkPage(Display display, MarketerView marketerView) {
	this.mv = marketerView;

	// this.official = official;
	this.display = display;
	Device device = Display.getCurrent();
	this.background_color = new Color(device, 200, 220, 200);

	// initUI();
    }

    void initUI() {
	this.mc = this.mv.getController();
	Device device = Display.getCurrent();

	final Shell shell = new Shell(display, SWT.SHELL_TRIM | SWT.CENTER);

	shell.setLayout(new FormLayout());

	shell.setBackground(background_color);

	// initUI widget definitions

	Label title_label = new Label(shell, SWT.CENTER);
	Font header_lable_font = new Font(title_label.getDisplay(),
		new FontData("Arial", 12, SWT.BOLD));
	title_label.setFont(header_lable_font);
	title_label.setText("Marketing Site");
	title_label.setBackground(background_color);

	sign_table = new Table(shell, SWT.BORDER);
	sign_table.setHeaderVisible(true);
	sign_table.setLinesVisible(true);

	String[] titles = { "Name", "Location", "Area Type", "Type", "price" };

	for (int i = 0; i < titles.length; i++) {
	    TableColumn column = new TableColumn(sign_table, SWT.NULL);
	    column.setText(titles[i]);
	    column.setWidth(130);
	}
	// TODO add all signs from the database to the table. Imidiate messege
	// get yes if publisher have privilage for it and no otherwhise.

	Label sign_name_label = new Label(shell, SWT.CENTER);
	sign_name_label.setText("no sign selsected");

	Label sign_location_label = new Label(shell, SWT.CENTER);
	sign_location_label.setText("");

	Label set_price_label = new Label(shell, SWT.CENTER);
	set_price_label.setText("price:");

	final Text set_price_text = new Text(shell, SWT.CENTER);
	set_price_text.setEnabled(false);
	set_price_text
		.setToolTipText("enter a number grater or equal to zero.");

	final Button change_button = new Button(shell, SWT.PUSH);
	change_button.setText("change");
	change_button.setEnabled(false);

	// initUI Handle Listeners

	// table selection listener
	sign_table.addListener(SWT.Selection, new Listener() {
	    public void handleEvent(Event e) {
		set_price_text.setEnabled(true);
		change_button.setEnabled(true);
		
	    }
	});

	// change button press listener
	change_button.addListener(SWT.Selection, new Listener() {
	    public void handleEvent(Event e) {
		String price_inupt = set_price_text.getText();
		boolean is_empty = isEmpty(price_inupt);
		if (is_empty) {
		    InputBoxEmpty(shell);
		}
		boolean is_numeric = isNumeric(price_inupt);
		if (!is_numeric) {
		    wrongInputdBox(shell);
		}
		double pricing = Double.parseDouble(price_inupt);
		String signName = sign_table.getSelection()[0].getText(0);
		mc.setSignPrice(signName, pricing);
		shell.pack();
	    }
	});

	// initUI Formating

	FormData title_label_data = new FormData();
	title_label_data.top = new FormAttachment(0, 20);
	title_label_data.left = new FormAttachment(40, 0);
	title_label.setLayoutData(title_label_data);

	FormData sign_table_data = new FormData();
	sign_table_data.top = new FormAttachment(title_label, 10);
	sign_table_data.left = new FormAttachment(0, 10);
	sign_table.setLayoutData(sign_table_data);

	FormData sign_name_label_data = new FormData();
	sign_name_label_data.top = new FormAttachment(sign_table, 10);
	sign_name_label_data.left = new FormAttachment(0, 15);
	sign_name_label.setLayoutData(sign_name_label_data);

	FormData sign_location_label_data = new FormData();
	sign_location_label_data.top = new FormAttachment(sign_table, 10);
	sign_location_label_data.left = new FormAttachment(sign_name_label, 10);
	sign_location_label.setLayoutData(sign_location_label_data);

	FormData set_price_label_data = new FormData();
	set_price_label_data.top = new FormAttachment(sign_table, 10);
	set_price_label_data.left = new FormAttachment(sign_location_label, 10);
	set_price_label.setLayoutData(set_price_label_data);

	FormData set_price_text_data = new FormData();
	set_price_text_data.top = new FormAttachment(sign_table, 10);
	set_price_text_data.left = new FormAttachment(set_price_label, 10);
	set_price_text.setLayoutData(set_price_text_data);

	FormData statistics_button_data = new FormData();
	statistics_button_data.top = new FormAttachment(sign_table, 15);
	statistics_button_data.right = new FormAttachment(100, -20);

	FormData change_button_data = new FormData();
	change_button_data.top = new FormAttachment(set_price_text, 10);
	change_button_data.left = new FormAttachment(0, 10);
	change_button_data.bottom = new FormAttachment(100, -20);
	change_button.setLayoutData(change_button_data);

	// loginUI run initialization

	shell.setText("Manager Site");
	shell.pack();
	shell.open();
	mc.registerMarketingListener(this);
	mv.onRefreshSigns();
	while (!shell.isDisposed()) {
	    if (!display.readAndDispatch())
		display.sleep();
	}
	mc.unregisterMarketingListener(this);
    }

    private void wrongInputdBox(Shell shell) {

	int style = SWT.ICON_INFORMATION | SWT.OK;

	MessageBox failed = new MessageBox(shell, style);
	failed.setText("wrong input");
	failed.setMessage("Error: Input must be a non negative number");
	failed.open();
    }

    private void InputBoxEmpty(Shell shell) {

	int style = SWT.ICON_INFORMATION | SWT.OK;

	MessageBox failed = new MessageBox(shell, style);
	failed.setText("wrong input");
	failed.setMessage(
		"Error: You must enter a number to the price text field");
	failed.open();
    }

    private static boolean isNumeric(String str) {
	return str.matches("\\d+(\\.\\d+)?|.{0}");
    }

    private static boolean isEmpty(String str) {
	return (str.matches(".{0}"));
    }

   
    @Override
    public void onSignAdded(Sign sign) {
	if (sign_table != null && !sign_table.isDisposed())
	    addSign(sign.getSignName(), sign.getAreaName(), sign.getAreaType(),
		    sign.signcanIamge(), sign_table);

    }

    public void addSign(String name, String location, AreaType areaType,
	    boolean canImage, Table sign_table) {
	if (sign_table == null || sign_table.isDisposed())
	    return;
	TableItem sign = new TableItem(sign_table, SWT.NULL);
	sign.setText(0, name);
	sign.setText(1, location);
	sign.setText(2, areaType.toString());
	sign.setText(3, String.valueOf(canImage));
	sign_table.pack();

    }

    @Override
    public void onSignRemoved(Sign sign) {
	if (sign_table != null && !sign_table.isDisposed())
	    removeSign(sign, sign_table);
    }

    private void removeSign(Sign sign, Table sign_table) {
	int index = getValueIndex(sign_table, 0, sign.getSignName());
	if (index > -1)
	    sign_table.remove(index);
    }

    @Override
    public void onSignEdited(Sign sign) {
	if (sign_table != null && !sign_table.isDisposed())
	    editSign(sign, sign_table);

    }

    private void editSign(Sign sign, Table sign_table) {
	int index = getValueIndex(sign_table, 0, sign.getSignName());
	TableItem item = sign_table.getItem(index);
	item.setText(1, sign.getAreaName());
	item.setText(2, sign.getAreaType().toString());
	item.setText(3, sign.getCanImage() ? "Image or text" : "Text only");
	item.setText(4, Double.toString(sign.getPricing()));
    }

    public void refreshSigns(ArrayList<Sign> signs, Table sign_table) {
	sign_table.removeAll();
	for (Sign s : signs) {
	    addSign(s.getSignName(), s.getAreaName(), s.getAreaType(), true,
		    sign_table);

	}
    }

    public void refreshSigns(List<Sign> signs) {
	sign_table.removeAll();
	for (Sign s : signs) {
	    addSign(s.getSignName(), s.getAreaName(), s.getAreaType(), true,
		    s.getPricing());
	}
    }

    public void addSign(String name, String location, AreaType areaType,
	    boolean canImage, double pricing) {
	if (sign_table == null)
	    return;
	TableItem sign = new TableItem(sign_table, SWT.NULL);
	sign.setText(0, name);
	sign.setText(1, location);
	sign.setText(2, areaType.toString());
	sign.setText(3, canImage ? "Image or text" : "Text only");
	sign.setText(4, Double.toString(pricing));
    }

    private int getValueIndex(Table table, int column, String value) {
	if (table == null || table.isDisposed())
	    return -1;
	TableItem[] ti = table.getItems();
	int index = -1;
	for (TableItem t : ti) {
	    String tableValue = t.getText(column);
	    if (tableValue.equals(value))
		index = table.indexOf(t);
	}
	return index;
    }

}
