package se.esigns.view.publisher;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.widgets.Display;

import se.esigns.controller.publisher.PublisherController;
import se.esigns.model.commercial.Commercial;
import se.esigns.model.publisher.IPublisherDatabase;
import se.esigns.model.publisher.IPublisherDatabaseListener;
import se.esigns.model.publisher.PType;
import se.esigns.model.publisher.Publisher;
import se.esigns.model.publisher.Statistics;
import se.esigns.model.sign.ISignsDatabaseListener;
import se.esigns.model.sign.Sign;

public class PublisherView {
    private PublisherController pc;
    private List<IPublisherDatabaseListener> dataListeners = new ArrayList<>();
    private PublisherMainWorkArea work_area;
    private PublisherLogin login;
    private Display display;
    private boolean official;

    public PublisherView(Display display) {
	this.display=display;
	login = new PublisherLogin(display, this);
    }

    public void initUI() {
	
	if (!pc.isLoggedIn())
	    login.initUI();
	else {
	    onLogin();
	}
    }

    public void registerController(PublisherController controller) {
	pc = controller;
    }

    public PublisherController getController() {
	return pc;
    }

    public void onLogin() {
	work_area = new PublisherMainWorkArea(display,
		pc.getPublisher().isOfficial(), this);
	login.onLogin();
	dataListeners.remove(login);
	dataListeners.add(work_area);
	work_area.initUI();
    }

    public void onLoginFail(String reason) {
	login.onLoginFailed(reason);
    }

    public void onLogout() {

    }

    public void onRegister() {
	login.onRegister();
    }

    public void onRegisterFail(String reason) {
	login.onRegisterFailed(reason);
    }

}
