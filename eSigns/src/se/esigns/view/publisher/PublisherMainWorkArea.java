package se.esigns.view.publisher;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.management.ImmutableDescriptor;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Device;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

import se.esigns.controller.publisher.PublisherController;
import se.esigns.model.commercial.Commercial;
import se.esigns.model.commercial.MediaCommercial;
import se.esigns.model.commercial.OneTimePolicy;
import se.esigns.model.commercial.PeriodicPolicy;
import se.esigns.model.commercial.Policy;
import se.esigns.model.commercial.TextCommercial;
import se.esigns.model.publisher.IPublisherDatabaseListener;
import se.esigns.model.publisher.OfficialPublisher;
import se.esigns.model.publisher.PType;
import se.esigns.model.publisher.Publisher;
import se.esigns.model.publisher.Statistics;
import se.esigns.model.publisher.CommercialStats;
import se.esigns.model.publisher.SignStats;
import se.esigns.model.sign.AreaType;
import se.esigns.model.sign.ISignsDatabaseListener;
import se.esigns.model.sign.Sign;
import se.esigns.view.sign.SignView;

public class PublisherMainWorkArea
	implements IPublisherDatabaseListener, ISignsDatabaseListener {

    Display display;
    Shell mainShell;
    boolean official;
    Color background_color;
    private PublisherView pv;
    private PublisherController pc;
    private Publisher p;
    int HOME_CELLS = 3;

    String NO_NAME_ERROR = "\tCommercial is missing a name.\n";
    String NO_SIGNS_ERROR = "\tNo signs where chosen.\n";
    String NO_TYPE_ERROR = "\tCommercial type was not chosen.\n";
    String NO_TEXT_ERROR = "\tCommercial missing text.\n";
    String NO_IMAGE_ERROR = "\tCommercial missing image adress.\n";
    String NO_POLICY_ERROR = "\tCommercial desplay policy was not chosen.\n";
    String NO_START_TIME_ERROR = "\tNo starting date or hour chosen.\n";
    String NO_WAIT_ERROR = "\tNo number of days to wait between broadcasts chosen.\n";
    String NO_END_TIME_ERROR = "\tNo ending date chosen.\n";
    String START_BEFORE_NOW_ERROR = "\tThe starting time chosen have passed.\n";
    String END_BEFOR_START_ERROR = "\tThe end time chosen preceed the start time.\n";

    public PublisherMainWorkArea(Display display, boolean official,
	    PublisherView pv) {
	this.display = display;
	this.official = official;
	this.display = display;
	Device device = Display.getCurrent();
	this.background_color = new Color(device, 220, 218, 170);
	this.pv = pv;

	// initUI();
    }

    public void initUI() {
	this.pc = pv.getController();
	this.p = pc.getPublisher();
	Device device = Display.getCurrent();

	mainShell = new Shell(display, SWT.SHELL_TRIM | SWT.CENTER);

	mainShell.setLayout(new FormLayout());

	mainShell.setBackground(background_color);

	// shell.setLayout(new GridLayout(2,false));

	// initUI widget definitions

	final Composite work_area = new Composite(mainShell, SWT.CENTER);
	work_area.setBackground(background_color);
	// work_area.setSize(WORK_AREA_WIDTH, WORK_AREA_HEIGHT);
	InitHomeArea(work_area, mainShell);

	Composite menue = new Composite(mainShell, SWT.CENTER);
	menue.setBackground(background_color);
	menue.setLayout(new GridLayout(1, false));

	Button home_button = new Button(menue, SWT.PUSH);
	home_button.setText("Home");

	Button add_commercail_button = new Button(menue, SWT.PUSH);
	add_commercail_button.setText("add new commercial");

	Button check_balance_button = new Button(menue, SWT.PUSH);
	check_balance_button.setText("Balance Status");

	Button simulation_button = new Button(menue, SWT.PUSH);
	simulation_button.setText("Sign simulation");

	// initUI Handle Listeners

	home_button.addListener(SWT.Selection, new Listener() {
	    public void handleEvent(Event e) {

		Control[] children = work_area.getChildren();
		for (int i = 0; i < children.length; i++) {
		    children[i].dispose();
		}

		InitHomeArea(work_area, mainShell);
		ArrayList<Commercial> commercials = new ArrayList<>(
			pc.getCommercials().values());
		refreshCommercials(commercials);
		work_area.pack();
		mainShell.pack();

	    }
	});

	add_commercail_button.addListener(SWT.Selection, new Listener() {
	    public void handleEvent(Event e) {

		Control[] children = work_area.getChildren();
		for (int i = 0; i < children.length; i++) {
		    children[i].dispose();
		}

		InitNewCommercailArea(mainShell, work_area);
		ArrayList<Sign> l = new ArrayList<>(pc.getSigns().values());
		refreshSigns(l, sign_table);
		refreshLocations(pc.getAreas());
		mainShell.pack();
	    }
	});

	check_balance_button.addListener(SWT.Selection, new Listener() {
	    public void handleEvent(Event e) {

		InitBalanceStatusArea(mainShell);
	    }
	});

	simulation_button.addListener(SWT.Selection, new Listener() {
	    public void handleEvent(Event e) {

		Control[] children = work_area.getChildren();
		for (int i = 0; i < children.length; i++) {
		    children[i].dispose();
		}

		InitSimulationArea(work_area);
		Map<String, Sign> dataSigns = pc.getSigns();
		HashSet<Sign> signs = new HashSet<>();
		for (Commercial c : pc.getCommercials().values())
		    for (String signName : c.getSignesSet())
			signs.add(dataSigns.get(signName));
		refreshSigns(new ArrayList<Sign>(signs), simulation_sign_table);
		mainShell.pack();
	    }
	});

	// initUI Formating
	FormData work_area_data = new FormData();
	work_area_data.top = new FormAttachment(0, 4);
	work_area_data.left = new FormAttachment(0, 4);
	work_area_data.right = new FormAttachment(90, -5);
	work_area.setLayoutData(work_area_data);

	FormData menue_data = new FormData();
	menue_data.top = new FormAttachment(0, 4);
	menue_data.left = new FormAttachment(work_area, 5);
	menue.setLayoutData(menue_data);

	GridData single_cell_data = new GridData(SWT.FILL, 0, true, true);

	home_button.setLayoutData(single_cell_data);
	add_commercail_button.setLayoutData(single_cell_data);
	check_balance_button.setLayoutData(single_cell_data);
	simulation_button.setLayoutData(single_cell_data);

	// initUI run initialization

	mainShell.setText("Publisher Site");
	mainShell.pack();
	mainShell.open();
	pc.registerPublisherListener(this);
	while (!mainShell.isDisposed()) {
	    if (!display.readAndDispatch())
		display.sleep();
	}
	pc.unregisterPublisherListener(this);
	pc.logout();
    }

    // ::::::::::::::::::::::::::
    // component functions
    // ::::::::::::::::::::::::::

    protected void refreshLocations(Set<String> areas) {
	// TODO Auto-generated method stub
	for (String area : areas) {
	    TableItem loc = new TableItem(area_table, SWT.NULL);
	    loc.setText(0, area);
	}

    }

    Table commercial_table;

    private void InitHomeArea(final Composite work_area, final Shell shell) {
	work_area.setLayout(new FormLayout());

	// InitHomeArea widget definitions

	Label title_label = new Label(work_area, SWT.CENTER);
	Font header_lable_font = new Font(title_label.getDisplay(),
		new FontData("Arial", 12, SWT.BOLD));
	title_label.setFont(header_lable_font);
	title_label.setText("My commercial list");
	title_label.setBackground(background_color);

	commercial_table = new Table(work_area, SWT.BORDER);
	commercial_table.setHeaderVisible(true);
	commercial_table.setLinesVisible(true);

	String[] titles = { "Name", "Type", "broadcast time", "signs",
		"price" };
	for (int i = 0; i < titles.length; i++) {
	    TableColumn column = new TableColumn(commercial_table, SWT.NULL);
	    column.setText(titles[i]);
	    column.setWidth(130);
	}

	final Button edit_button = new Button(work_area, SWT.PUSH);
	edit_button.setText("edit");
	edit_button.setEnabled(false);

	final Button remove_button = new Button(work_area, SWT.PUSH);
	remove_button.setText("remove");
	remove_button.setEnabled(false);

	final Button Statistic_button = new Button(work_area, SWT.PUSH);
	Statistic_button.setText("view statistics");
	Statistic_button.setEnabled(false);

	// InitHomeArea Handle Listeners

	commercial_table.addListener(SWT.Selection, new Listener() {
	    public void handleEvent(Event e) {
		edit_button.setEnabled(true);
		remove_button.setEnabled(true);
		Statistic_button.setEnabled(true);
	    }
	});

	edit_button.addListener(SWT.Selection, new Listener() {
	    public void handleEvent(Event e) {

	    }
	});

	remove_button.addListener(SWT.Selection, new Listener() {
	    public void handleEvent(Event e) {

	    }
	});

	Statistic_button.addListener(SWT.Selection, new Listener() {
	    public void handleEvent(Event e) {

		String selected_sign = commercial_table.getSelection()[0]
			.getText(0);

		Control[] children = work_area.getChildren();
		for (int i = 0; i < children.length; i++) {
		    children[i].dispose();
		}
		current_commercial_stats = selected_sign;
		InitStatisticArea(work_area, selected_sign);
		shell.pack();
	    }
	});

	// InitHomeArea Formating

	FormData title_label_data = new FormData();
	title_label_data.top = new FormAttachment(0, 10);
	title_label_data.left = new FormAttachment(40, 0);
	title_label.setLayoutData(title_label_data);

	FormData commercial_table_data = new FormData();
	commercial_table_data.top = new FormAttachment(title_label, 10);
	commercial_table_data.left = new FormAttachment(0, 10);
	commercial_table.setLayoutData(commercial_table_data);

	FormData edit_button_data = new FormData();
	edit_button_data.top = new FormAttachment(commercial_table, 10);
	edit_button_data.left = new FormAttachment(0, 10);
	edit_button.setLayoutData(edit_button_data);

	FormData remove_button_data = new FormData();
	remove_button_data.top = new FormAttachment(commercial_table, 10);
	remove_button_data.left = new FormAttachment(edit_button, 80);
	remove_button.setLayoutData(remove_button_data);

	FormData Statistic_button_data = new FormData();
	Statistic_button_data.top = new FormAttachment(edit_button, 30);
	Statistic_button_data.left = new FormAttachment(0, 10);
	Statistic_button.setLayoutData(Statistic_button_data);

    }

    Table sign_table;
    Table area_table;
    Table chosen_sign_table;
    Button imidiate_radio_button;
    Label immidiate_commercial_lable;

    private void InitNewCommercailArea(final Shell shell,
	    final Composite work_area) {
	work_area.setLayout(new FormLayout());

	// InitNewCommercailArea widget definitions

	Label title_label = new Label(work_area, SWT.CENTER);
	Font header_lable_font = new Font(title_label.getDisplay(),
		new FontData("Arial", 12, SWT.BOLD));
	title_label.setFont(header_lable_font);
	title_label.setText("New Commercial");
	title_label.setBackground(background_color);

	Label commercail_name_label = new Label(work_area, SWT.CENTER);
	commercail_name_label.setText("Commercial name: ");
	final Text commercail_name_text = new Text(work_area, SWT.LEFT);

	Composite sign_tables_Cpmposite = new Composite(work_area, SWT.CENTER);
	sign_tables_Cpmposite.setBackground(background_color);
	sign_tables_Cpmposite.setLayout(new FormLayout());

	sign_table = new Table(sign_tables_Cpmposite, SWT.BORDER | SWT.MULTI);
	sign_table.setHeaderVisible(true);
	sign_table.setLinesVisible(true);

	String[] sign_titles = { "Name", "Location", "Area Type", "Type",
		"price" };
	for (int i = 0; i < sign_titles.length; i++) {
	    TableColumn column = new TableColumn(sign_table, SWT.NULL);
	    column.setText(sign_titles[i]);
	    column.setWidth(130);
	}

	area_table = new Table(sign_tables_Cpmposite, SWT.BORDER | SWT.MULTI);
	area_table.setHeaderVisible(true);
	area_table.setLinesVisible(true);

	String[] area_titles = { "Location" };
	for (int i = 0; i < area_titles.length; i++) {
	    TableColumn column = new TableColumn(area_table, SWT.NULL);
	    column.setText(area_titles[i]);
	    column.setWidth(130);
	}

	chosen_sign_table = new Table(work_area, SWT.BORDER);
	chosen_sign_table.setHeaderVisible(true);
	chosen_sign_table.setLinesVisible(true);

	for (int i = 0; i < sign_titles.length; i++) {
	    TableColumn column = new TableColumn(chosen_sign_table, SWT.NULL);
	    column.setText(sign_titles[i]);
	    column.setWidth(130);
	}
	// chosen_sign_table.setEnabled(false);

	int price = 0; // TODO read full price.
	Label price_label = new Label(work_area, SWT.CENTER);
	price_label.setText("final price: " + price);

	Composite type_radial_text_composite = new Composite(work_area,
		SWT.CENTER);
	type_radial_text_composite.setBackground(background_color);
	type_radial_text_composite.setLayout(new RowLayout(SWT.VERTICAL));

	Label text_ad_label = new Label(type_radial_text_composite, SWT.CENTER);
	text_ad_label.setText("text advertisment");

	Label image_ad_label = new Label(type_radial_text_composite,
		SWT.CENTER);
	image_ad_label.setText("image advertisment");

	Composite type_radial_button_composite = new Composite(work_area,
		SWT.CENTER);
	type_radial_button_composite.setBackground(background_color);
	type_radial_button_composite.setLayout(new RowLayout(SWT.VERTICAL));

	final Button text_radio_button = new Button(
		type_radial_button_composite, SWT.RADIO);
	final Button image_radio_button = new Button(
		type_radial_button_composite, SWT.RADIO);

	Label ad_text_label = new Label(work_area, SWT.CENTER);
	ad_text_label.setText("Advertisment text: ");

	final Text ad_text = new Text(work_area, SWT.MULTI);
	ad_text.setEnabled(false);

	Label ad_image_label = new Label(work_area, SWT.CENTER);
	ad_image_label.setText("Advertisment image: ");

	final Button braws_button = new Button(work_area, SWT.PUSH);
	braws_button.setText("browse");
	braws_button.setEnabled(false);

	final Text ad_image_adress_text = new Text(work_area, SWT.LEFT);
	ad_image_adress_text.setEnabled(false);

	Composite times_radial_text_composite = new Composite(work_area,
		SWT.CENTER);
	times_radial_text_composite.setBackground(background_color);
	times_radial_text_composite.setLayout(new RowLayout(SWT.VERTICAL));

	Label singel_commercial_lable = new Label(times_radial_text_composite,
		SWT.CENTER);
	singel_commercial_lable.setText("single date");

	Label reacurring_commercial_lable = new Label(
		times_radial_text_composite, SWT.CENTER);
	reacurring_commercial_lable.setText("reacurring Commercial");

	immidiate_commercial_lable = new Label(times_radial_text_composite,
		SWT.CENTER);
	immidiate_commercial_lable.setText("immidiate Commercial");
	immidiate_commercial_lable.setVisible(official);

	Composite times_radial_buttons_composite = new Composite(work_area,
		SWT.CENTER);
	times_radial_buttons_composite.setBackground(background_color);
	times_radial_buttons_composite.setLayout(new RowLayout(SWT.VERTICAL));

	// imidiate_radio_button = null;
	final Button single_radio_button = new Button(
		times_radial_buttons_composite, SWT.RADIO);
	final Button reaccuring_radio_button = new Button(
		times_radial_buttons_composite, SWT.RADIO);
	// if (official) {
	imidiate_radio_button = new Button(times_radial_buttons_composite,
		SWT.RADIO);
	imidiate_radio_button.setVisible(official);
	// }

	Label time_lable = new Label(work_area, SWT.CENTER);
	time_lable.setText("Choose time for the advertisment to play: ");
	time_lable.setBackground(background_color);

	final Combo time_list = new Combo(work_area, SWT.BORDER);
	time_list.setEnabled(false);
	for (int i = 0; i < 24; i++) {
	    if (i < 10) {
		time_list.add("0" + i + ":00");
		time_list.add("0" + i + ":30");
	    } else {
		time_list.add(i + ":00");
		time_list.add(i + ":30");
	    }
	}

	Label start_date_lable = new Label(work_area, SWT.CENTER);
	start_date_lable.setText("Choose date for the advertisment to play: ");
	start_date_lable.setBackground(background_color);

	Label start_day_lable = new Label(work_area, SWT.CENTER);
	start_day_lable.setText("day");
	start_day_lable.setBackground(background_color);

	final Combo start_day_list = new Combo(work_area, SWT.BORDER);
	start_day_list.setEnabled(false);
	for (int i = 1; i < 32; i++) {
	    start_day_list.add("" + i);
	}

	Label start_month_lable = new Label(work_area, SWT.CENTER);
	start_day_lable.setText("month");
	start_month_lable.setBackground(background_color);

	final Combo start_month_list = new Combo(work_area, SWT.BORDER);
	start_month_list.setEnabled(false);
	for (int i = 1; i < 13; i++) {
	    start_month_list.add("" + i);
	}

	Label start_year_lable = new Label(work_area, SWT.CENTER);
	start_year_lable.setText("year");
	start_year_lable.setBackground(background_color);

	final Combo start_year_list = new Combo(work_area, SWT.BORDER);
	start_year_list.setEnabled(false);
	for (int i = 2015; i < 2116; i++) {
	    start_year_list.add("" + i);
	}

	Label repeat_days_label = new Label(work_area, SWT.CENTER);
	repeat_days_label.setText("number of days between repeats: ");
	repeat_days_label.setBackground(background_color);

	final Text repeat_days_text = new Text(work_area, SWT.LEFT);
	// repeat_days_text.setEditable(false); for some reason the text field
	// isn't enabling if I use this command here.

	Label end_date_label = new Label(work_area, SWT.CENTER);
	end_date_label.setText("The advertisment will play until: ");
	end_date_label.setBackground(background_color);

	Label end_day_label = new Label(work_area, SWT.CENTER);
	end_day_label.setText("day");
	end_day_label.setBackground(background_color);

	final Combo end_day_list = new Combo(work_area, SWT.BORDER);
	end_day_list.setEnabled(false);
	for (int i = 1; i < 32; i++) {
	    end_day_list.add("" + i);
	}

	Label end_month_label = new Label(work_area, SWT.CENTER);
	end_month_label.setText("month");
	end_month_label.setBackground(background_color);

	final Combo end_month_list = new Combo(work_area, SWT.BORDER);
	end_month_list.setEnabled(false);
	for (int i = 1; i < 13; i++) {
	    end_month_list.add("" + i);
	}

	Label end_year_label = new Label(work_area, SWT.CENTER);
	end_year_label.setText("year");
	end_year_label.setBackground(background_color);

	final Combo end_year_list = new Combo(work_area, SWT.BORDER);
	end_year_list.setEnabled(false);
	for (int i = 2015; i < 2116; i++) {
	    end_year_list.add("" + i);
	}

	final Button accept_button = new Button(work_area, SWT.PUSH);
	accept_button.setText("accept");

	// InitNewCommercailArea Handle Listeners

	sign_table.addListener(SWT.Selection, new Listener() {
	    public void handleEvent(Event e) {
		final TableItem[] selected_sign = sign_table.getSelection();
		String[] sign_data = new String[5];
		for (int i = 0; i < sign_data.length; i++) {
		    sign_data[i] = selected_sign[0].getText(i);
		}
		TableItem[] chosen_sign_list = chosen_sign_table.getItems();
		int remove_index = -1;
		for (int i = 0; i < chosen_sign_list.length; i++) {
		    if (chosen_sign_list[i].getText(0).matches(sign_data[0])) {
			remove_index = i;
		    }
		}
		if (remove_index < 0) { // if the index is negative add.
		    TableItem new_item = new TableItem(chosen_sign_table,
			    SWT.NONE);
		    new_item.setText(sign_data);
		    shell.pack();
		}
	    }
	});

	area_table.addListener(SWT.Selection, new Listener() {
	    public void handleEvent(Event e) {
		// TODO: select signs by location
		TableItem[] sign_list = sign_table.getItems();
		TableItem[] selected_area = area_table.getSelection();
		for (int i = 0; i < sign_list.length; i++) {
		    if (sign_list[i].getText(1)
			    .equals(selected_area[0].getText(0))) {
			final TableItem selected_sign = sign_list[i];
			String[] sign_data = new String[5];
			for (int j = 0; j < sign_data.length; j++) {
			    sign_data[j] = selected_sign.getText(j);
			}
			TableItem[] chosen_sign_list = chosen_sign_table
				.getItems();
			int remove_index = -1;
			for (int j = 0; j < chosen_sign_list.length; j++) {
			    if (chosen_sign_list[j].getText(0)
				    .matches(sign_data[0])) {
				remove_index = j;
			    }
			}
			if (remove_index < 0) { // if the index is negative add.
			    TableItem new_item = new TableItem(
				    chosen_sign_table, SWT.NONE);
			    new_item.setText(sign_data);
			    shell.pack();
			}
		    }
		}
	    }
	});

	chosen_sign_table.addListener(SWT.Selection, new Listener() {
	    public void handleEvent(Event e) {
		chosen_sign_table.remove(chosen_sign_table.getSelectionIndex());
		// chosen_sign_table.pack();
		// work_area.pack();
		shell.pack();
	    }
	});

	text_radio_button.addListener(SWT.Selection, new Listener() {
	    public void handleEvent(Event e) {

		ad_text.setEnabled(true);
		braws_button.setEnabled(false);
		ad_image_adress_text.setEnabled(false);
	    }
	});

	image_radio_button.addListener(SWT.Selection, new Listener() {
	    public void handleEvent(Event e) {

		ad_text.setEnabled(false);
		braws_button.setEnabled(true);
		ad_image_adress_text.setEnabled(true);
	    }
	});

	single_radio_button.addListener(SWT.Selection, new Listener() {
	    public void handleEvent(Event e) {
		time_list.setEnabled(true);
		start_day_list.setEnabled(true);
		start_month_list.setEnabled(true);
		start_year_list.setEnabled(true);
		repeat_days_text.setEnabled(false);
		end_day_list.setEnabled(false);
		end_month_list.setEnabled(false);
		end_year_list.setEnabled(false);
	    }
	});

	reaccuring_radio_button.addListener(SWT.Selection, new Listener() {
	    public void handleEvent(Event e) {
		time_list.setEnabled(true);
		start_day_list.setEnabled(true);
		start_month_list.setEnabled(true);
		start_year_list.setEnabled(true);
		repeat_days_text.setEnabled(true);
		end_day_list.setEnabled(true);
		end_month_list.setEnabled(true);
		end_year_list.setEnabled(true);
	    }
	});

	imidiate_radio_button.addListener(SWT.Selection, new Listener() {
	    public void handleEvent(Event e) {
		time_list.setEnabled(false);
		start_day_list.setEnabled(false);
		start_month_list.setEnabled(false);
		start_year_list.setEnabled(false);
		repeat_days_text.setEnabled(false);
		end_day_list.setEnabled(false);
		end_month_list.setEnabled(false);
		end_year_list.setEnabled(false);
	    }
	});

	braws_button.addListener(SWT.Selection, new Listener() {
	    public void handleEvent(Event e) {
		FileDialog image_dialog = new FileDialog(shell, SWT.OPEN);
		image_dialog.setFilterExtensions(new String[] { "*.jpg" });
		image_dialog.setFilterPath("c:\\");
		String result = image_dialog.open();
		ad_image_adress_text.setText(result);
	    }
	});

	accept_button.addListener(SWT.Selection, new Listener() {
	    public void handleEvent(Event e) {

		boolean wrong_input = false;
		String error_messege = "";
		String commercial_name = commercail_name_text.getText();
		String commercial_text = ad_text.getText();
		String image_adress = ad_image_adress_text.getText();

		if (commercial_name.matches("")) {
		    wrong_input = true;
		    error_messege += NO_NAME_ERROR;
		}

		if (chosen_sign_table.getItems().length == 0) {
		    wrong_input = true;
		    error_messege += NO_SIGNS_ERROR;
		}

		if (!(text_radio_button.getSelection()
			|| image_radio_button.getSelection())) {
		    wrong_input = true;
		    error_messege += NO_TYPE_ERROR;
		}

		if (text_radio_button.getSelection()
			&& commercial_text.matches("")) {
		    wrong_input = true;
		    error_messege += NO_TEXT_ERROR;
		}

		if (image_radio_button.getSelection()
			&& image_adress.matches("")) {
		    wrong_input = true;
		    error_messege += NO_IMAGE_ERROR;
		}

		Policy policy = null;
		boolean pushSelected = imidiate_radio_button.getSelection();
		if (!(single_radio_button.getSelection()
			|| reaccuring_radio_button.getSelection()
			|| pushSelected)) {
		    wrong_input = true;
		    error_messege += NO_POLICY_ERROR;
		} else {
		    if (single_radio_button.getSelection()) {

			Calendar start_time = Calendar.getInstance();
			if ((start_year_list.getText().matches(""))
				|| (start_month_list.getText().matches(""))
				|| (start_day_list.getText().matches(""))
				|| (time_list.getText().matches(""))) {
			    wrong_input = true;
			    error_messege += NO_START_TIME_ERROR;
			} else if (single_radio_button.getSelection()) {
			    start_time.set(Calendar.YEAR, Integer
				    .parseInt(start_year_list.getText()));
			    start_time.set(Calendar.MONTH, Integer
				    .parseInt(start_month_list.getText())-1);
			    start_time.set(Calendar.DAY_OF_MONTH,
				    Integer.parseInt(start_day_list.getText()));
			    start_time.set(Calendar.HOUR_OF_DAY,
				    Integer.parseInt(time_list.getText()
					    .substring(0, 2)));
			    start_time.set(Calendar.MINUTE, Integer.parseInt(
				    time_list.getText().substring(3, 5)));
			    start_time.set(Calendar.SECOND, 0);
			    start_time.set(Calendar.MILLISECOND, 0);

			    if (!pushSelected && start_time
				    .before(Calendar.getInstance())) {
				System.out.println(start_time.getTime());
				wrong_input = true;
				error_messege += START_BEFORE_NOW_ERROR;
			    }
			    policy = new OneTimePolicy(start_time.getTime());

			}
			//
		    } else if (reaccuring_radio_button.getSelection()) {

			Calendar start_time = Calendar.getInstance();
			if ((start_year_list.getText().matches(""))
				|| (start_month_list.getText().matches(""))
				|| (start_day_list.getText().matches(""))
				|| (time_list.getText().matches(""))) {
			    wrong_input = true;
			    error_messege += NO_START_TIME_ERROR;
			} else {
			    start_time.set(Calendar.YEAR, Integer
				    .parseInt(start_year_list.getText()));
			    start_time.set(Calendar.MONTH, Integer
				    .parseInt(start_month_list.getText())-1);
			    start_time.set(Calendar.DAY_OF_MONTH,
				    Integer.parseInt(start_day_list.getText()));
			    start_time.set(Calendar.HOUR_OF_DAY,
				    Integer.parseInt(time_list.getText()
					    .substring(0, 2)));
			    start_time.set(Calendar.MINUTE, Integer.parseInt(
				    time_list.getText().substring(3, 5)));
			    start_time.set(Calendar.SECOND, 0);
			    start_time.set(Calendar.MILLISECOND, 0);
			}

			if (repeat_days_text.getText().matches("")) {
			    wrong_input = true;
			    error_messege += NO_WAIT_ERROR;
			}

			if (!pushSelected
				&& start_time.before(Calendar.getInstance())) {
			    wrong_input = true;
			    error_messege += START_BEFORE_NOW_ERROR;
			}

			Calendar end_time = Calendar.getInstance();
			if ((end_year_list.getText().matches(""))
				|| (end_month_list.getText().matches(""))
				|| (end_day_list.getText().matches(""))) {
			    wrong_input = true;
			    error_messege += NO_END_TIME_ERROR;
			} else {
			    end_time.set(Calendar.YEAR,
				    Integer.parseInt(end_year_list.getText()));
			    end_time.set(Calendar.MONTH,
				    Integer.parseInt(end_month_list.getText())-1);
			    end_time.set(Calendar.DAY_OF_MONTH,
				    Integer.parseInt(end_day_list.getText()));
			    end_time.set(Calendar.HOUR_OF_DAY, Integer.parseInt(
				    time_list.getText().substring(0, 2)));
			    end_time.set(Calendar.MINUTE, Integer.parseInt(
				    time_list.getText().substring(3, 5)));
			    end_time.set(Calendar.SECOND, 0);
			    end_time.set(Calendar.MILLISECOND, 0);
			}

			if (end_time.before(start_time)) {
			    wrong_input = true;
			    error_messege += END_BEFOR_START_ERROR;
			}
			int repeat_days = Integer
				.parseInt(repeat_days_text.getText());
			System.out.println("************\nStart time: "
				+ start_time.getTime().toString()
				+ " End time: " + end_time.getTime().toString()
				+ " Repeat: " + repeat_days + "\n************");
			policy = new PeriodicPolicy(start_time.getTime(),
				end_time.getTime(), repeat_days);
		    } else {
			policy = new OneTimePolicy(
				Calendar.getInstance().getTime());
		    }
		}

		if (wrong_input) {
		    addSignError(shell, error_messege);
		    return;
		}

		Set<Sign> publishedIn = new HashSet<Sign>();
		Set<String> signNames = new HashSet<String>();
		for (TableItem t : chosen_sign_table.getItems())
		    signNames.add(t.getText(0));
		for (String signName : signNames)
		    publishedIn.add(pc.getSigns().get(signName));
		Commercial publishable_commercial;
		if (text_radio_button.getSelection()) {
		    publishable_commercial = new TextCommercial(commercial_name,
			    policy, p.getUsername(), publishedIn,
			    commercial_text);
		    // publishable_commercial.setId(commercial_name);

		} else {
		    publishable_commercial = new MediaCommercial(
			    commercial_name, policy, p.getUsername(),
			    publishedIn, image_adress);

		}
		if (imidiate_radio_button.getSelection()) {
		    OfficialPublisher op = (OfficialPublisher) pc
			    .getPublisher();
		    pc.pushText(publishable_commercial);
		} else
		    pc.publish(publishable_commercial);
	    }
	});

	// InitNewCommercailArea Formating
	FormData title_label_data = new FormData();
	title_label_data.top = new FormAttachment(0, 10);
	title_label_data.left = new FormAttachment(40, 0);
	title_label.setLayoutData(title_label_data);

	FormData commercail_name_label_data = new FormData();
	commercail_name_label_data.top = new FormAttachment(title_label, 10);
	commercail_name_label_data.left = new FormAttachment(0, 10);
	commercail_name_label.setLayoutData(commercail_name_label_data);

	FormData commercail_name_text_data = new FormData();
	commercail_name_text_data.top = new FormAttachment(title_label, 10);
	commercail_name_text_data.left = new FormAttachment(
		commercail_name_label, 10);
	commercail_name_text_data.right = new FormAttachment(30, 0);
	commercail_name_text.setLayoutData(commercail_name_text_data);

	FormData sign_tables_Cpmposite_data = new FormData();
	sign_tables_Cpmposite_data.top = new FormAttachment(
		commercail_name_label, 10);
	sign_tables_Cpmposite_data.left = new FormAttachment(0, 10);
	sign_tables_Cpmposite.setLayoutData(sign_tables_Cpmposite_data);

	FormData area_table_data = new FormData();
	area_table_data.top = new FormAttachment(0, 10);
	area_table_data.left = new FormAttachment(0, 1);
	area_table.setLayoutData(area_table_data);

	FormData sign_table_data = new FormData();
	sign_table_data.top = new FormAttachment(0, 10);
	sign_table_data.left = new FormAttachment(area_table, 10);
	sign_table.setLayoutData(sign_table_data);

	FormData chosen_sign_table_data = new FormData();
	chosen_sign_table_data.top = new FormAttachment(sign_tables_Cpmposite,
		10);
	chosen_sign_table_data.left = new FormAttachment(0, 10);
	chosen_sign_table.setLayoutData(chosen_sign_table_data);

	FormData price_label_data = new FormData();
	price_label_data.top = new FormAttachment(chosen_sign_table, 10);
	price_label_data.left = new FormAttachment(0, 10);
	price_label.setLayoutData(price_label_data);

	FormData type_radial_text_composite_data = new FormData();
	type_radial_text_composite_data.top = new FormAttachment(price_label,
		10);
	type_radial_text_composite_data.left = new FormAttachment(0, 100);
	type_radial_text_composite
		.setLayoutData(type_radial_text_composite_data);

	FormData type_radial_button_composite_data = new FormData();
	type_radial_button_composite_data.top = new FormAttachment(price_label,
		10);
	type_radial_button_composite_data.left = new FormAttachment(
		type_radial_text_composite, 10);
	type_radial_button_composite
		.setLayoutData(type_radial_button_composite_data);

	FormData ad_text_label_data = new FormData();
	ad_text_label_data.top = new FormAttachment(price_label, 10);
	ad_text_label_data.left = new FormAttachment(
		type_radial_button_composite, 50);
	ad_text_label.setLayoutData(ad_text_label_data);

	FormData ad_text_data = new FormData();
	ad_text_data.top = new FormAttachment(price_label, 10);
	ad_text_data.left = new FormAttachment(ad_text_label, 30);
	ad_text_data.right = new FormAttachment(80, 0);
	ad_text_data.height = 80;
	ad_text.setLayoutData(ad_text_data);

	FormData ad_image_label_data = new FormData();
	ad_image_label_data.top = new FormAttachment(ad_text, 10);
	ad_image_label_data.left = new FormAttachment(
		type_radial_button_composite, 10);
	ad_image_label.setLayoutData(ad_image_label_data);

	FormData braws_button_data = new FormData();
	braws_button_data.top = new FormAttachment(ad_text, 10);
	braws_button_data.left = new FormAttachment(ad_image_label, 15);
	braws_button.setLayoutData(braws_button_data);

	FormData ad_image_adress_text_data = new FormData();
	ad_image_adress_text_data.top = new FormAttachment(ad_text, 10);
	ad_image_adress_text_data.left = new FormAttachment(braws_button, 5);
	ad_image_adress_text_data.right = new FormAttachment(80, 0);
	ad_image_adress_text.setLayoutData(ad_image_adress_text_data);

	FormData times_radial_text_composite_data = new FormData();
	times_radial_text_composite_data.top = new FormAttachment(
		ad_image_adress_text, 40);
	times_radial_text_composite_data.left = new FormAttachment(0, 100);
	times_radial_text_composite
		.setLayoutData(times_radial_text_composite_data);

	FormData times_radial_buttons_composite_data = new FormData();
	times_radial_buttons_composite_data.top = new FormAttachment(
		ad_image_adress_text, 40);
	times_radial_buttons_composite_data.left = new FormAttachment(
		times_radial_text_composite, 10);
	times_radial_buttons_composite
		.setLayoutData(times_radial_buttons_composite_data);

	FormData single_time_lable_data = new FormData();
	single_time_lable_data.top = new FormAttachment(ad_image_adress_text,
		10);
	single_time_lable_data.left = new FormAttachment(
		times_radial_buttons_composite, 50);
	time_lable.setLayoutData(single_time_lable_data);

	FormData time_list_data = new FormData();
	time_list_data.top = new FormAttachment(ad_image_adress_text, 10);
	time_list_data.left = new FormAttachment(time_lable, 10);
	time_list.setLayoutData(time_list_data);

	FormData start_date_lable_data = new FormData();
	start_date_lable_data.top = new FormAttachment(time_list, 10);
	start_date_lable_data.left = new FormAttachment(
		times_radial_buttons_composite, 10);
	start_date_lable.setLayoutData(start_date_lable_data);

	FormData start_day_lable_data = new FormData();
	start_day_lable_data.top = new FormAttachment(time_list, 10);
	start_day_lable_data.left = new FormAttachment(start_date_lable, 10);
	start_day_lable.setLayoutData(start_day_lable_data);

	FormData start_day_list_data = new FormData();
	start_day_list_data.top = new FormAttachment(time_list, 10);
	start_day_list_data.left = new FormAttachment(start_day_lable, 10);
	start_day_list.setLayoutData(start_day_list_data);

	FormData start_month_lable_data = new FormData();
	start_month_lable_data.top = new FormAttachment(time_list, 10);
	start_month_lable_data.left = new FormAttachment(start_day_list, 10);
	start_month_lable.setLayoutData(start_month_lable_data);

	FormData start_month_list_data = new FormData();
	start_month_list_data.top = new FormAttachment(time_list, 10);
	start_month_list_data.left = new FormAttachment(start_month_lable, 10);
	start_month_list.setLayoutData(start_month_list_data);

	FormData start_year_lable_data = new FormData();
	start_year_lable_data.top = new FormAttachment(time_list, 10);
	start_year_lable_data.left = new FormAttachment(start_month_list, 10);
	start_year_lable.setLayoutData(start_year_lable_data);

	FormData start_year_list_data = new FormData();
	start_year_list_data.top = new FormAttachment(time_list, 10);
	start_year_list_data.left = new FormAttachment(start_year_lable, 10);
	start_year_list.setLayoutData(start_year_list_data);

	FormData repeat_days_label_data = new FormData();
	repeat_days_label_data.top = new FormAttachment(start_date_lable, 10);
	repeat_days_label_data.left = new FormAttachment(
		times_radial_buttons_composite, 10);
	repeat_days_label.setLayoutData(repeat_days_label_data);

	FormData repeat_days_text_data = new FormData();
	repeat_days_text_data.top = new FormAttachment(start_date_lable, 10);
	repeat_days_text_data.left = new FormAttachment(repeat_days_label, 10);
	repeat_days_text.setLayoutData(repeat_days_text_data);

	FormData end_date_lable_data = new FormData();
	end_date_lable_data.top = new FormAttachment(repeat_days_text, 10);
	end_date_lable_data.left = new FormAttachment(
		times_radial_buttons_composite, 10);
	end_date_label.setLayoutData(end_date_lable_data);

	FormData end_day_lable_data = new FormData();
	end_day_lable_data.top = new FormAttachment(repeat_days_text, 10);
	end_day_lable_data.left = new FormAttachment(end_date_label, 10);
	end_day_label.setLayoutData(end_day_lable_data);

	FormData end_day_list_data = new FormData();
	end_day_list_data.top = new FormAttachment(repeat_days_text, 10);
	end_day_list_data.left = new FormAttachment(end_day_label, 10);
	end_day_list.setLayoutData(end_day_list_data);

	FormData end_month_label_data = new FormData();
	end_month_label_data.top = new FormAttachment(repeat_days_text, 10);
	end_month_label_data.left = new FormAttachment(end_day_list, 10);
	end_month_label.setLayoutData(end_month_label_data);

	FormData end_month_list_data = new FormData();
	end_month_list_data.top = new FormAttachment(repeat_days_text, 10);
	end_month_list_data.left = new FormAttachment(end_month_label, 10);
	end_month_list.setLayoutData(end_month_list_data);

	FormData end_year_label_data = new FormData();
	end_year_label_data.top = new FormAttachment(repeat_days_text, 10);
	end_year_label_data.left = new FormAttachment(end_month_list, 10);
	end_year_label.setLayoutData(end_year_label_data);

	FormData end_year_list_data = new FormData();
	end_year_list_data.top = new FormAttachment(repeat_days_text, 10);
	end_year_list_data.left = new FormAttachment(end_year_label, 10);
	end_year_list.setLayoutData(end_year_list_data);

	FormData accept_button_data = new FormData();
	accept_button_data.top = new FormAttachment(end_year_list, 40);
	accept_button_data.bottom = new FormAttachment(100, -10);
	accept_button_data.left = new FormAttachment(0, 120);
	accept_button.setLayoutData(accept_button_data);
    }

    private void InitBalanceStatusArea(Shell shell) {
	int style = SWT.ICON_INFORMATION | SWT.OK;

	MessageBox balance = new MessageBox(shell, style);
	balance.setText("Balance");
	double sum = pc.getPublisher().getBalance(); // TODO read sum from user.
	balance.setMessage("sum required for payment:\n\n\t" + sum + " NIS.");
	balance.open();
    }

    Table simulation_sign_table;

    private void InitSimulationArea(Composite work_area) {
	work_area.setLayout(new FormLayout());

	Label title_label = new Label(work_area, SWT.CENTER);
	Font header_lable_font = new Font(title_label.getDisplay(),
		new FontData("Arial", 12, SWT.BOLD));
	title_label.setFont(header_lable_font);
	title_label.setText("Sign simulation");
	title_label.setBackground(background_color);

	Label instructions_label = new Label(work_area, SWT.CENTER);
	instructions_label.setText("Choose sign to simulate.");
	instructions_label.setBackground(background_color);

	simulation_sign_table = new Table(work_area, SWT.BORDER);
	simulation_sign_table.setHeaderVisible(true);
	simulation_sign_table.setLinesVisible(true);

	String[] titles = { "Name", "Location", "Area Type", "Type" };
	for (int i = 0; i < titles.length; i++) {
	    TableColumn column = new TableColumn(simulation_sign_table,
		    SWT.NULL);
	    column.setText(titles[i]);
	    column.setWidth(130);
	}

	final Button simulate_button = new Button(work_area, SWT.PUSH);
	simulate_button.setText("simulate");
	simulate_button.setEnabled(false);

	// InitHomeArea Handle Listeners
	simulation_sign_table.addListener(SWT.Selection, new Listener() {
	    public void handleEvent(Event e) {
		simulate_button.setEnabled(true);
	    }
	});
	simulate_button.addListener(SWT.Selection, new Listener() {
	    public void handleEvent(Event e) {
		String signName = simulation_sign_table.getSelection()[0]
			.getText(0);
		Sign sign = pc.getSigns().get(signName);
		SignView sView = new SignView(display, pc, sign);
		sView.simulateSign();
	    }
	});

	// InitHomeArea Formating
	FormData title_label_data = new FormData();
	title_label_data.top = new FormAttachment(0, 10);
	title_label_data.left = new FormAttachment(40, 0);
	title_label.setLayoutData(title_label_data);

	FormData instructions_label_data = new FormData();
	instructions_label_data.top = new FormAttachment(title_label, 10);
	instructions_label_data.left = new FormAttachment(10, 0);
	instructions_label.setLayoutData(instructions_label_data);

	FormData sign_table_data = new FormData();
	sign_table_data.top = new FormAttachment(instructions_label, 10);
	sign_table_data.left = new FormAttachment(0, 10);
	simulation_sign_table.setLayoutData(sign_table_data);

	FormData simulate_button_data = new FormData();
	simulate_button_data.top = new FormAttachment(simulation_sign_table,
		10);
	simulate_button_data.left = new FormAttachment(15, 0);
	simulate_button.setLayoutData(simulate_button_data);
    }

    int x = 0, y = 0;
    Label stats_label;
    Table stats_sign_table;
    Label final_display_lable;
    Label final_charge_lable;

    private void InitStatisticArea(Composite work_area,
	    String commercial_name) {
	work_area.setLayout(new FormLayout());

	Statistics statistics = p.getPublisherStatistics();

	Label title_label = new Label(work_area, SWT.CENTER);
	Font header_lable_font = new Font(title_label.getDisplay(),
		new FontData("Arial", 12, SWT.BOLD));
	title_label.setFont(header_lable_font);
	title_label.setText("Statistic view");
	title_label.setBackground(background_color);

	stats_label = new Label(work_area, SWT.CENTER);
	stats_label.setBackground(background_color);
	stats_label.setText("no stats yet");

	stats_sign_table = new Table(work_area, SWT.BORDER);
	stats_sign_table.setHeaderVisible(true);
	stats_sign_table.setLinesVisible(true);

	String[] titles = { "sign name", "Location", "Area Type",
		"display time", "charge" };
	for (int i = 0; i < titles.length; i++) {
	    TableColumn column = new TableColumn(stats_sign_table, SWT.NULL);
	    column.setText(titles[i]);
	    column.setWidth(130);
	}

	final_display_lable = new Label(work_area, SWT.CENTER);
	final_display_lable.setBackground(background_color);

	final_charge_lable = new Label(work_area, SWT.CENTER);
	final_charge_lable.setBackground(background_color);

	updateCommercialStats(commercial_name, statistics);

	// InitHomeArea Handle Listeners

	// InitHomeArea Formating
	FormData title_label_data = new FormData();
	title_label_data.top = new FormAttachment(0, 10);
	title_label_data.left = new FormAttachment(40, 0);
	title_label.setLayoutData(title_label_data);

	FormData stats_label_data = new FormData();
	stats_label_data.top = new FormAttachment(title_label, 150);
	stats_label_data.left = new FormAttachment(0, 100);
	stats_label.setLayoutData(stats_label_data);

	FormData sign_table_data = new FormData();
	sign_table_data.top = new FormAttachment(title_label, 10);
	sign_table_data.left = new FormAttachment(0, 10);
	stats_sign_table.setLayoutData(sign_table_data);

	FormData final_display_lable_data = new FormData();
	final_display_lable_data.top = new FormAttachment(stats_sign_table, 10);
	final_display_lable_data.left = new FormAttachment(0, 20);
	final_display_lable.setLayoutData(final_display_lable_data);

	FormData final_charge_lable_data = new FormData();
	final_charge_lable_data.top = new FormAttachment(final_display_lable,
		2);
	final_charge_lable_data.left = new FormAttachment(0, 20);
	final_charge_lable.setLayoutData(final_charge_lable_data);
	pc.registerPublisherListener(this);
    }

    private void updateCommercialStats(String commercial_name,
	    Statistics statistics) {
	HashMap<String, SignStats> stat_list = statistics.commercialsPublished
		.getOrDefault(commercial_name,
			new CommercialStats()).publishedInSigns;

	Map<String, Sign> sign_map = pc.getSigns();

	for (Map.Entry<String, SignStats> e : stat_list.entrySet()) {
	    TableItem table_line = new TableItem(stats_sign_table, SWT.NONE);
	    Sign s = sign_map.get(e.getKey());
	    table_line.setText(0, s.getSignName());
	    table_line.setText(1, s.getAreaName());
	    table_line.setText(2, s.getAreaType().toString());
	    table_line.setText(3, String.valueOf(e.getValue().signDisplayTime));
	    table_line.setText(4, String.valueOf(e.getValue().signCharge));
	}
	int final_display_time = statistics.commercialsPublished.getOrDefault(
		commercial_name, new CommercialStats()).displayTime;

	int final_charge = statistics.commercialsPublished
		.getOrDefault(commercial_name, new CommercialStats()).charged;

	final_display_lable
		.setText("overall display time: " + final_display_time);
	final_charge_lable.setText("overall price charged: " + final_charge);
    }

    void addSignError(Shell shell, String errors) {
	String messege = "sign was not added please fix the following issues:\n"
		+ errors;
	int style = SWT.ICON_ERROR | SWT.OK;
	MessageBox dia = new MessageBox(shell, style);
	dia.setText("Error");
	dia.setMessage(messege);
	dia.open();
    }

    @Override
    public void onPublisherModelUpdate(Publisher publisher) {
	if (imidiate_radio_button == null || imidiate_radio_button.isDisposed())
	    return;
	if (publisher == null) {
	    mainShell.dispose();
	    pv.onLogin();
	}
	boolean status = pc.getPublisher().isOfficial();
	if (official != status) {
	    imidiate_radio_button.setVisible(status);
	    imidiate_radio_button.redraw();
	    immidiate_commercial_lable.setVisible(status);
	    immidiate_commercial_lable.redraw();
	    // official_radio
	    official = status;
	}
    }

    private int getValueIndex(Table table, int column, String value) {
	if (table == null || table.isDisposed())
	    return -1;
	TableItem[] ti = table.getItems();
	int index = -1;
	for (TableItem t : ti) {
	    String tableValue = t.getText(column);
	    if (tableValue.equals(value))
		index = table.indexOf(t);
	}
	return index;
    }

    @Override
    public void onSignAdded(Sign sign) {
	if (sign_table != null && !sign_table.isDisposed())
	    addSign(sign.getSignName(), sign.getAreaName(), sign.getAreaType(),
		    sign.signcanIamge(), sign_table);
	if (simulation_sign_table != null
		&& !simulation_sign_table.isDisposed())
	    addSign(sign.getSignName(), sign.getAreaName(), sign.getAreaType(),
		    sign.signcanIamge(), simulation_sign_table);
    }

    public void addSign(String name, String location, AreaType areaType,
	    boolean canImage, Table sign_table) {
	if (sign_table == null || sign_table.isDisposed())
	    return;
	TableItem sign = new TableItem(sign_table, SWT.NULL);
	sign.setText(0, name);
	sign.setText(1, location);
	sign.setText(2, areaType.toString());
	sign.setText(3, String.valueOf(canImage));
	// sign_table.pack();

    }

    @Override
    public void onSignRemoved(Sign sign) {
	if (sign_table != null && !sign_table.isDisposed())
	    removeSign(sign, sign_table);
	if (simulation_sign_table != null
		&& !simulation_sign_table.isDisposed())
	    removeSign(sign, simulation_sign_table);
    }

    private void removeSign(Sign sign, Table sign_table) {
	int index = getValueIndex(sign_table, 0, sign.getSignName());
	if (index > -1)
	    sign_table.remove(index);
    }

    @Override
    public void onSignEdited(Sign sign) {
	if (sign_table != null && !sign_table.isDisposed())
	    editSign(sign, sign_table);
	if (simulation_sign_table != null
		&& !simulation_sign_table.isDisposed())
	    editSign(sign, simulation_sign_table);
    }

    private void editSign(Sign sign, Table sign_table) {
	if (sign_table != null && !sign_table.isDisposed()) {
	    int index = getValueIndex(sign_table, 0, sign.getSignName());
	    TableItem item = sign_table.getItem(index);
	    item.setText(1, sign.getAreaName());
	    item.setText(2, sign.getAreaType().toString());
	    item.setText(3, sign.getCanImage() ? "Image or text" : "Text only");
	}
    }

    public void refreshSigns(ArrayList<Sign> signs, Table sign_table) {
	sign_table.removeAll();
	for (Sign s : signs) {
	    addSign(s.getSignName(), s.getAreaName(), s.getAreaType(), true,
		    sign_table);

	}
    }

    public void refreshCommercials(ArrayList<Commercial> commercials) {
	commercial_table.removeAll();
	for (Commercial c : commercials) {
	    onCommercialAdded(c);

	}
    }

    @Override
    public void onCommercialAdded(Commercial commercial) {

	if (commercial_table == null || commercial_table.isDisposed())
	    return;
	TableItem comm = new TableItem(commercial_table, SWT.NULL);
	comm.setText(0, commercial.getName());
	comm.setText(1,
		commercial instanceof MediaCommercial ? "Image" : "Text");
	comm.setText(2, commercial.policy.getStartDate().toString());
	StringBuffer buff = new StringBuffer();
	for (String s : commercial.getSignesSet())
	    buff.append(s);
	comm.setText(3, buff.toString());
	// commercial_table.pack();
    }

    @Override
    public void onCommercialRemoved(Commercial commercial) {
	// TODO Auto-generated method stub

    }

    String current_commercial_stats = "";

    public void simulateStats(final Statistics stats) {
	// System.out
	// .println("Sign stats updated: " + stats.toString());
	// if (sign)
	Runnable runnable = new Runnable() {
	    @Override
	    public void run() {
		if (!stats_label.isDisposed()) {
		    stats_sign_table.removeAll();
		    updateCommercialStats(current_commercial_stats, stats);
		}
	    }
	};
	display.syncExec(runnable);
    }

    @Override
    public void onPublisherStatsUpdate(Statistics stats, Publisher publisher) {
	// TODO Auto-generated method stub
	if (stats_sign_table == null || stats_sign_table.isDisposed())
	    return;
	/******************
	 * PRINTING STATISTICS
	 ***************************************/

	System.out.println(stats.toString());

	/**********************************************************/

	simulateStats(stats);
    }

    @Override
    public void onNotifyPublisher(String message) {
	int style = SWT.ICON_INFORMATION | SWT.OK;

	MessageBox balance = new MessageBox(mainShell, style);
	double balance_amount = pc.getPublisher().getBalance();
	balance.setText("Balance");
	int sum = 0; // TODO read sum from user.
	balance.setMessage("Message from the marketing department: " + message);
	balance.open();
    }

}
