package se.esigns.view.publisher;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import se.esigns.controller.publisher.PublisherController;
import se.esigns.model.publisher.IPublisherDatabaseListener;
import se.esigns.model.publisher.Publisher;
import se.esigns.model.sign.Sign;

public class PublisherLogin  {

	PublisherView publisherView;
	PublisherController pc;
	Display display;
	public PublisherLogin(Display display, PublisherView publisherView) {    
        this.display=display;
        this.publisherView=publisherView;
        
    }
	
	 Shell   loginShell ;
	 Text login_username_text;
	 Text login_password_text;
    private void loginUI() {
	this.pc=publisherView.getController();
         loginShell = new Shell(display, SWT.DIALOG_TRIM | SWT.CENTER);
        loginShell.setLayout(new FormLayout());
        
      //loginUI widget definitions        
              
        Label header_lable = new Label(loginShell, SWT.LEFT);
        Font header_lable_font = new Font(header_lable.getDisplay(), new FontData("Arial", 12, SWT.BOLD));
        header_lable.setFont(header_lable_font);
        header_lable.setText("Login");
        
        Label explenation_lable = new Label(loginShell, SWT.LEFT);
        explenation_lable.setText("Log in or press the sign up to create a new account.");
        
        Label login_lable = new Label(loginShell, SWT.LEFT);
        login_lable.setText("Enter user name:");
        
        Label login_password_lable = new Label(loginShell, SWT.LEFT);
        login_password_lable.setText("Enter user password:");
        
        login_username_text = new Text(loginShell, SWT.SINGLE);
        login_username_text.setTextLimit(200);
        login_username_text.setToolTipText("Publisher user name must be up to 200 characters");
        
        login_password_text = new Text(loginShell, SWT.SINGLE);
        login_password_text.setTextLimit(200);
        login_password_text.setToolTipText("Publisher user password must be up to 200 characters");
        
        
        Button login_button = new Button(loginShell, SWT.PUSH);
        Font login_button_font = new Font(login_button.getDisplay(), new FontData("Arial", 10, SWT.BOLD));
        login_button.setFont(login_button_font);
        login_button.setText("Log In");
        login_button.addListener(SWT.Selection, new Listener() {
        	public void handleEvent(Event e) {
        	    	String username=login_username_text.getText();
        	    	String password=login_password_text.getText();
        		pc.setUsername(username);
        		pc.setPassword(password);
        		pc.login();
//        		    loginSuccessBox(); 
        		//else loginFailedBox(loginShell, username, "Username or password incorrect");
        	    	
                }
        });
        Button signup_button = new Button(loginShell, SWT.PUSH);
        Font signup_button_font = new Font(signup_button.getDisplay(), new FontData("Arial", 10, SWT.BOLD));
        signup_button.setFont(signup_button_font);
        signup_button.setText("Sign Up");
        signup_button.addListener(SWT.Selection, new Listener() {
        	public void handleEvent(Event e) {
        		loginShell.dispose();
        		signInUI(display);
                }
        });


        //loginUI Formating        
        
        FormData header_label_data = new FormData();
        header_label_data.top = new FormAttachment(0, 10);
        header_label_data.left = new FormAttachment(40, 0);
        header_lable.setLayoutData(header_label_data);
        
        FormData explenation_label_data = new FormData();
        explenation_label_data.top = new FormAttachment(header_lable, 10);
        explenation_label_data.left = new FormAttachment(0, 10);
        explenation_label_data.right = new FormAttachment(100, -40);
        explenation_lable.setLayoutData(explenation_label_data);
        
        FormData login_label_data = new FormData();
        login_label_data.top = new FormAttachment(explenation_lable, 10);
        login_label_data.left = new FormAttachment(0, 10);
        login_lable.setLayoutData(login_label_data);
        
        FormData login_password_label_data = new FormData();
        login_password_label_data.top = new FormAttachment(login_lable, 20);
        login_password_label_data.left = new FormAttachment(0, 10);
        login_password_lable.setLayoutData(login_password_label_data);
        
        
        FormData login_username_text_data = new FormData();
        login_username_text_data.top = new FormAttachment(explenation_lable, 10);
        login_username_text_data.left = new FormAttachment(login_lable, 15);
        login_username_text_data.width = 120;
        login_username_text_data.right = new FormAttachment(100, -20);
        login_username_text.setLayoutData(login_username_text_data);
        
        FormData login_password_text_data = new FormData();
        login_password_text_data.top = new FormAttachment(login_lable, 20);
        login_password_text_data.left = new FormAttachment(login_password_lable, 15);
        login_password_text_data.width = 120;
        login_password_text_data.right = new FormAttachment(100, -20);
        login_password_text.setLayoutData(login_password_text_data);
        
        
        
        FormData login_button_data = new FormData();
        login_button_data.top = new FormAttachment(login_password_lable, 15);
        login_button_data.left = new FormAttachment(5, 10);
        login_button.setLayoutData(login_button_data);
        
        FormData signup_button_data = new FormData();
        signup_button_data.top = new FormAttachment(login_password_lable, 15);
        signup_button_data.right = new FormAttachment(95, -10);
        signup_button.setLayoutData(signup_button_data);

		//loginUI run initialization
        
        loginShell.setText("Login");
        loginShell.pack();
        loginShell.open();
        
        while (!loginShell.isDisposed()) {
            if (!display.readAndDispatch())
                display.sleep();
        }        
    }

    Shell signUpUIshell;
    private void signInUI(final Display display) {
        
        signUpUIshell = new Shell(display, SWT.DIALOG_TRIM | SWT.CENTER);
        signUpUIshell.setLayout(new FormLayout());
        
        //signInUI widget definitions        

        Label header_lable = new Label(signUpUIshell, SWT.LEFT);
        Font header_lable_font = new Font(header_lable.getDisplay(), new FontData("Arial", 12, SWT.BOLD));
        header_lable.setFont(header_lable_font);
        header_lable.setText("Sign Up");
        
        Label explenation_lable = new Label(signUpUIshell, SWT.LEFT);
        explenation_lable.setText("Enter Company name to register to the system.");
        
        Label signup_username_lable = new Label(signUpUIshell, SWT.LEFT);
        signup_username_lable.setText("Username:");
        
        Label signup_password_lable = new Label(signUpUIshell, SWT.LEFT);
        signup_password_lable.setText("Password:");
        
        
        final Text signup_username_text = new Text(signUpUIshell, SWT.SINGLE);
        signup_username_text.setTextLimit(200);
        signup_username_text.setToolTipText("Publisher Company name must be up to 200 characters");
        
        final Text signup_password_text = new Text(signUpUIshell, SWT.SINGLE);
        signup_password_text.setTextLimit(200);
        signup_password_text.setToolTipText("Publisher password name must be up to 200 characters");
        
        
        Button ok_button = new Button(signUpUIshell, SWT.PUSH);
        Font login_button_font = new Font(ok_button.getDisplay(), new FontData("Arial", 10, SWT.BOLD));
        ok_button.setFont(login_button_font);
        ok_button.setText("OK");
        ok_button.addListener(SWT.Selection, new Listener() {
        	public void handleEvent(Event e) {
        		String username = signup_username_text.getText();
        		String password = signup_password_text.getText();
        		pc.setUsername(username);
        		pc.setPassword(password);
        		pc.register();
        		
        		signUpUIshell.dispose();
        		
        		loginUI();
                }
        });
        
        Button cancel_button = new Button(signUpUIshell, SWT.PUSH);
        Font signup_button_font = new Font(cancel_button.getDisplay(), new FontData("Arial", 10, SWT.BOLD));
        cancel_button.setFont(signup_button_font);
        cancel_button.setText("Cancel");
        cancel_button.addListener(SWT.Selection, new Listener() {
        	public void handleEvent(Event e) {
        	    signUpUIshell.dispose();
        		loginUI();
                }
        });

        //signInUI Formating        
        
        FormData header_label_data = new FormData();
        header_label_data.top = new FormAttachment(0, 10);
        header_label_data.left = new FormAttachment(40, 0);
        header_lable.setLayoutData(header_label_data);
        
        FormData explenation_label_data = new FormData();
        explenation_label_data.top = new FormAttachment(header_lable, 10);
        explenation_label_data.left = new FormAttachment(0, 10);
        explenation_label_data.right = new FormAttachment(100, -40);
        explenation_lable.setLayoutData(explenation_label_data);
        
        FormData signup_username_lable_data = new FormData();
        signup_username_lable_data.top = new FormAttachment(explenation_lable, 10);
        signup_username_lable_data.left = new FormAttachment(0, 10);
        signup_username_lable.setLayoutData(signup_username_lable_data);
        
        FormData signup_password_lable_data = new FormData();
        signup_password_lable_data.top = new FormAttachment(signup_username_lable, 10);
        signup_password_lable_data.left = new FormAttachment(0, 10);
        signup_password_lable.setLayoutData(signup_password_lable_data);
        
        
        FormData signup_username_text_data = new FormData();
        signup_username_text_data.top = new FormAttachment(explenation_lable, 10);
        signup_username_text_data.left = new FormAttachment(signup_username_lable, 15);
        signup_username_text_data.width = 120;
        signup_username_text_data.right = new FormAttachment(100, -20);
        signup_username_text.setLayoutData(signup_username_text_data);
        
        FormData signup_password_text_data = new FormData();
        signup_password_text_data.top = new FormAttachment(signup_username_lable, 10);
        signup_password_text_data.left = new FormAttachment(signup_password_lable, 15);
        signup_password_text_data.width = 120;
        signup_password_text_data.right = new FormAttachment(100, -20);
        signup_password_text.setLayoutData(signup_password_text_data);
        
        FormData ok_button_data = new FormData();
        ok_button_data.top = new FormAttachment(signup_password_lable, 15);
        ok_button_data.left = new FormAttachment(20, 10);
        ok_button.setLayoutData(ok_button_data);
        
        FormData cancel_button_data = new FormData();
        cancel_button_data.top = new FormAttachment(signup_password_lable, 15);
        cancel_button_data.right = new FormAttachment(80, -10);
        cancel_button.setLayoutData(cancel_button_data);


		//signInUI run initialization     
        
        signUpUIshell.setText("placeholder");
        signUpUIshell.pack();
        signUpUIshell.open();
        
        while (!signUpUIshell.isDisposed()) {
            if (!display.readAndDispatch())
                display.sleep();
        }        
    }  
    
private void loginSuccessBox() {
        
        int style = SWT.ICON_INFORMATION | SWT.OK;

        MessageBox sucess = new MessageBox(loginShell, style);
        sucess.setText("Information");
        sucess.setMessage("Welcome "+login_username_text.getText()+" I missed you!"); 
        sucess.open();
    }
    
 private void loginFailedBox(String reason) {
        
        int style = SWT.ICON_INFORMATION | SWT.OK;

        MessageBox failed = new MessageBox(loginShell, style);
        failed.setText("Information");
        failed.setMessage("Login for user " + login_username_text.getText() + " failed.\n"+reason); 
        failed.open();
    }
    
    private void signUpSecessBox(Shell shell, String company_name) {
        
        int style = SWT.ICON_INFORMATION | SWT.OK;

        MessageBox sucess = new MessageBox(shell, style);
        sucess.setText("Information");
        sucess.setMessage("New Company Sucessfully registered as " + company_name + "."); 
        sucess.open();
    }
    
    private void signUpFailedBox(Shell shell, String company_name,String reason) {
        
        int style = SWT.ICON_INFORMATION | SWT.OK;

        MessageBox failed = new MessageBox(shell, style);
        failed.setText("Information");
        failed.setMessage("Registration for company " + company_name + " failed.\n"+reason); 
        failed.open();
    }
 
    public void onLogin(){
	loginSuccessBox();
	
	loginShell.dispose();
    }
    
    public void onLoginFailed(String reason){
	loginFailedBox(reason);
	loginShell.dispose();
	loginUI();
    }
 	
    public void onRegister(){
	signUpSecessBox(signUpUIshell,pc.getUsername());
    }
    public void onRegisterFailed(String reason){
	signUpFailedBox(signUpUIshell,pc.getUsername(),reason);
    }

    

    public void initUI() {
	// TODO Auto-generated method stub
	loginUI();
    }


}