package se.esigns.view.sign;

import java.util.Collection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Device;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

import se.esigns.controller.publisher.PublisherController;
import se.esigns.model.commercial.Commercial;
import se.esigns.model.commercial.TextCommercial;
import se.esigns.model.commercial.MediaCommercial;
import se.esigns.model.commercial.OneTimePolicy;
import se.esigns.model.publisher.Statistics;
import se.esigns.model.sign.ISignDataListener;
import se.esigns.model.sign.Sign;

public class SignView implements ISignDataListener {
	private Display display;
	Shell shell;
	Label sign_label;
	Label sign_name_label;
	Label sign_location_label;
	Label stats_label;
	private PublisherController pc;
	private Sign sign;
	private int DELAY = 10000;
	private int commercial_count;

	public SignView(Display display, PublisherController pc, Sign sign) {
		this.display = display;
		this.pc = pc;
		this.sign = sign;
		this.pc.registerSignListener(this, sign.getSignName());
	}

	public void simulateCommercial(final Commercial commercial) {
		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				if (!shell.isDisposed()) {
					sign_label.setText("default");
					if (commercial instanceof TextCommercial) {
						sign_label.setImage(null);
						sign_label.setText(((TextCommercial) commercial)
								.getText());
					} else {
						sign_label.setText("default");
						Image commercial_image = new Image(shell.getDisplay(),
								((MediaCommercial) commercial).getImageAdress());
						sign_label.setImage(commercial_image);
					}

					sign_label.redraw();
				}
			}
		};

		display.syncExec(runnable);
	}

	public void simulateStats(final Statistics stats) {
		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				if (!shell.isDisposed()) {
					sign_name_label.setText(sign.getSignName());
					sign_location_label.setText(sign.getAreaName());
					stats_label.setText("Sign stats updated: "
							+ stats.toString());
					stats_label.redraw();
					sign_name_label.redraw();
					sign_location_label.redraw();
				}
			}
		};

		display.syncExec(runnable);
	}

	public void simulateSign() {

		shell = new Shell(display, SWT.DIALOG_TRIM | SWT.CENTER);
//		shell.setLayout(new FillLayout());
		Device device = Display.getCurrent();
		Color simulation_background_color = new Color(device, 248, 248, 250);
		Color panel_background_color = new Color(device, 230, 230, 245);

		sign_label = new Label(shell, SWT.LEFT);
	    Font sign_lable_font = new Font(sign_label.getDisplay(), new FontData("Arial", 16, SWT.BOLD));
	    sign_label.setFont(sign_lable_font);
		sign_label.setBackground(simulation_background_color);
		sign_label.setBounds(0, 0, 800, 600);

		sign_name_label = new Label(shell, SWT.LEFT);
		sign_name_label.setBackground(panel_background_color);
		sign_name_label.setBounds(10, 600, 90, 20);
		
		sign_location_label = new Label(shell, SWT.LEFT);
		sign_location_label.setBackground(panel_background_color);
		sign_location_label.setBounds(100, 600, 700, 20);
		
		
		
		stats_label = new Label(shell, SWT.LEFT);
		stats_label.setBackground(panel_background_color);
		stats_label.setBounds(0, 620, 800, 120);

		shell.setText("Sign simulation");
		shell.setSize(800, 740);
		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}

	}

	// private ArrayList ArrayList(Collection<Commercial> values) {
	// // TODO Auto-generated method stub
	// return null;
	// }

	@Override
	public void onCommercialDiaplyed(Commercial commercial, Sign sign) {
		if (shell == null || shell.isDisposed())
			return;
		simulateCommercial(commercial);
		System.out.println(sign.getStats().commercialsPublished.toString());
	}

	@Override
	public void onStatsUpdate(Statistics stats, Sign sign) {
		// TODO Auto-generated method stub
		if (shell == null || shell.isDisposed())
			return;
		simulateStats(stats);
	}

}