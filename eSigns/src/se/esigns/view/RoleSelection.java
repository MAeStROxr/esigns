package se.esigns.view;

import javax.xml.bind.JAXBException;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;

import se.esigns.ApplicationContext;
import se.esigns.SignsApplication;
import se.esigns.controller.management.ManagerController;
import se.esigns.controller.management.MarketerController;
import se.esigns.controller.publisher.PublisherController;
import se.esigns.model.sign.AreaType;
import se.esigns.view.management.ManagerMainWorkPage;
import se.esigns.view.management.MarketingMainWorkPage;
import se.esigns.view.publisher.PublisherLogin;
import se.esigns.view.publisher.PublisherMainWorkArea;



public class RoleSelection {
    	ApplicationContext ctx=new ApplicationContext();
	SignsApplication app=ApplicationContext.getApp();
	public RoleSelection(Display display) {
        
        initUI(display);
    }
    
    private void initUI(final Display display) {
        
        Shell shell = new Shell(display, SWT.DIALOG_TRIM | SWT.CENTER);
        shell.setLayout(new FormLayout());
        
        //initUI widget definitions        

        
        Label top_lable = new Label(shell, SWT.LEFT);
        Font top_lable_font = new Font(top_lable.getDisplay(), new FontData("Arial", 12, 0));
        top_lable.setFont(top_lable_font);
        top_lable.setText("Wellcome to eSign.");
        		
        Label explenation_lable = new Label(shell, SWT.LEFT);
        Font explenation_lable_font = new Font(top_lable.getDisplay(), new FontData("Arial", 10, 0));
        top_lable.setFont(explenation_lable_font);
        explenation_lable.setText("\nplease choose wich role you wish to connect as.");
        
        Button customer_login_button = new Button(shell, SWT.PUSH);
        customer_login_button.setText("Publisher");
        customer_login_button.addListener(SWT.Selection, new Listener() {
        	public void handleEvent(Event e) {
        		PublisherController publisher =app.createPublisher(display);
//        		PublisherMainWorkArea pub_ui = new PublisherMainWorkArea(display, false);
        		publisher.initUI();
                }
        });
        
        Button marketing_login_button = new Button(shell, SWT.PUSH);
        marketing_login_button.setText("MarketerController Department");
        marketing_login_button.addListener(SWT.Selection, new Listener() {
        	public void handleEvent(Event e) {
        		MarketerController marketer = app.createMarketer(display);
        		marketer.initUI();
        	}
        });
        
        Button menager_login_button = new Button(shell, SWT.PUSH);
        menager_login_button.setText("Signs Menager");
        menager_login_button.addListener(SWT.Selection, new Listener() {
        	public void handleEvent(Event e) {
        		ManagerController manager = app.createManager(display);
        		manager.initUI();
        		/**/
        		  
        	
        	}
        });
        
        
		//initUI Formating        

        FormData top_data = new FormData();
        top_data.top = new FormAttachment(0, 10);
        top_data.left = new FormAttachment(30, 0);
        top_lable.setLayoutData(top_data);
        
        FormData explenation_data = new FormData();
        explenation_data.top = new FormAttachment(top_lable, 10);
        explenation_data.left = new FormAttachment(0, 5);
        explenation_lable.setLayoutData(explenation_data);
        
        FormData customer_data = new FormData();
        customer_data.top = new FormAttachment(explenation_lable, 5);
        customer_data.left = new FormAttachment(0, 15);
        customer_login_button.setLayoutData(customer_data);
        
        FormData marketing_data = new FormData();
        marketing_data.top = new FormAttachment(explenation_lable, 5);
        marketing_data.left = new FormAttachment(customer_login_button, 10);
        marketing_login_button.setLayoutData(marketing_data);
        
        FormData menager_data = new FormData();
        menager_data.top = new FormAttachment(explenation_lable, 5);
        menager_data.left = new FormAttachment(marketing_login_button, 10);
        menager_data.right = new FormAttachment(100, -15);
        menager_login_button.setLayoutData(menager_data);

		//initUI run initialization
        
        shell.setText("Welcome to eSign");
        shell.pack();
        shell.open();
        
        while (!shell.isDisposed()) {
            if (!display.readAndDispatch())
                display.sleep();
        }        
    }
   
    @SuppressWarnings("unused")
    public static void main(String[] args) throws JAXBException {
	ApplicationContext ctx=new ApplicationContext();
	ApplicationContext.createContext();
	SignsApplication app=ApplicationContext.getApp();
        app.start();
        Display display = new Display();
        RoleSelection log = new RoleSelection(display);
        app.close();
        display.dispose();    
        //chechking
    }
	
}
