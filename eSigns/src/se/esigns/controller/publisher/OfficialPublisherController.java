package se.esigns.controller.publisher;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.xml.bind.annotation.XmlTransient;

import se.esigns.model.commercial.OneTimePolicy;
import se.esigns.model.commercial.PeriodicPolicy;
import se.esigns.model.commercial.TextCommercial;
import se.esigns.model.publisher.IOfficialDatabase;
import se.esigns.model.sign.AreaType;
import se.esigns.model.sign.Sign;
import se.esigns.view.publisher.OfficialPublisherView;

public class OfficialPublisherController extends PublisherController {
    @XmlTransient
    public IOfficialDatabase officialsData;

    public OfficialPublisherController(IOfficialDatabase db,OfficialPublisherView view) {
	super(db,view);
	this.officialsData=db;
    }

    /**
     * 
     * @param TextCommercial
     */
    private void pushText(TextCommercial text, Set<Sign> signs) {
	for (Sign s : signs) {
	    officialsData.pushText(this, text, s);
	}
    }
 
    
    /**
	 * 
	 * @param TextCommercial
	 */
	public void pushText(TextCommercial text,String area) {
		
		Collection<Sign> signs=officialsData.getSigns().values();
		Set<Sign> toPublish=new HashSet<Sign>();
		for (Sign s : signs) if (s.getAreaName().equals(area)) 
			toPublish.add(s);
		pushText(text,toPublish);
		
	}
	/**
	 * 
	 * @param TextCommercial
	 */
	public void pushText(TextCommercial text,AreaType area) {
		
		Collection<Sign> signs=officialsData.getSigns().values();
		Set<Sign> toPublish=new HashSet<Sign>();
		for (Sign s : signs) {
		    if (s.getAreaType().equals(area)) 
			toPublish.add(s);
		}
			
		pushText(text,toPublish);
	}
	
}