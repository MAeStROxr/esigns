package se.esigns.controller.publisher;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import se.esigns.model.commercial.Commercial;
import se.esigns.model.commercial.MediaCommercial;
import se.esigns.model.commercial.PeriodicPolicy;
import se.esigns.model.commercial.TextCommercial;
import se.esigns.model.publisher.IPublisherDatabase;
import se.esigns.model.publisher.IPublisherDatabaseListener;
import se.esigns.model.publisher.Publisher;
import se.esigns.model.publisher.Statistics;
import se.esigns.model.sign.ISignDataListener;
import se.esigns.model.sign.ISignsDatabaseListener;
import se.esigns.model.sign.Sign;
import se.esigns.view.publisher.PublisherView;

public class PublisherController {

    private IPublisherDatabase data;
    PublisherView view;
    // TODO: this data should be in the module
    String username;
    String password;

    public PublisherController(IPublisherDatabase data, PublisherView view) {

	this.data = data;

	this.view = view;
	view.registerController(this);
    }

    public void setUsername(String username) {
	this.username = username;
    }

    public void setPassword(String password) {
	this.password = password;
    }

    public boolean isLoggedIn() {
	return data.isLoggedIn(this);
    }

    public boolean login() {
	if (data.isLoggedIn(this)) {
	    view.onLoginFail("User already logged in");
	    return false;
	}
	if (!data.login(this)) {
	    view.onLoginFail("Invalid username or password");
	    return false;
	}
	view.onLogin();
	return true;
    }

    public void registerPublisherListener(IPublisherDatabaseListener l) {
	data.registerPublisherDataListener(l, username, password);
    }

    public void unregisterPublisherListener(IPublisherDatabaseListener l) {
	data.unregisterPublisherDataListener(l, username);
    }

    public boolean logout() {
	if (!data.logout(this))
	    return false;
	view.onLogout();
	return true;
    }

    public boolean register() {
	if (!data.registerPublisher(this)) {
	    view.onRegisterFail("Username taken");
	    return false;
	}
	view.onRegister();
	return true;
    }

    // /**
    // *
    // * @param data
    // */
    // public void setSignsData(IPublisherDatabase data) {
    // this.data = data;
    // }

    // public MediaCommercial createMediaCommercial(String image, PeriodicPolicy
    // p,
    // Set<Sign> signs) {
    // // TODO:
    // String id = "getIdFromDatabase";
    // Publisher pub = data.getPublisher(username, password);
    // MediaCommercial m = new MediaCommercial(p, pub, signs, image);
    // return m;
    // }

    /**
     * 
     * @param Commercial
     * @param area
     */
    public void publish(TextCommercial commercial, String area) {
	Collection<Sign> signs = data.getSigns().values();
	Set<Sign> toPublish = new HashSet<Sign>();
	Set<String> signNames = new HashSet<>();
	for (Sign s : signs)
	    // TODO: CHECK THAT SIGN CAN SHOW MEDIA
	    if (s.getAreaName().equals(area))
		toPublish.add(s);
	
	for (Sign s : signs )
//	commercial.getSignesSet()
//		.addAll(toPublish.stream().map(x -> (x.getSignName());
	publish(commercial);
    }

    public void pushText(Commercial publishable_commercial) {
	data.push(this, publishable_commercial);
    }

    /**
     * 
     * @param Commercial
     * @param publishedInSigns
     */
    public void publish(Commercial commercial) {
	data.publish(this, commercial);
    }

    public String getUsername() {
	return username;
    }

    public String getPassword() {
	return password;
    }

    public void initUI() {
	// TODO Auto-generated method stub
	view.initUI();
    }

    public Map<String, Sign> getSigns() {
	return data.getSigns();
    }

    public Set<String> getAreas() {
	return data.getAreas();
    }

    public Publisher getPublisher() {
	return data.getPublisher(username, password);
    }

    public Map<String, Commercial> getCommercials() {
	return data.getCommercials(this);
    }

    public void registerSignListener(ISignDataListener signView,
	    String signName) {
	// TODO Auto-generated method stub
	data.registerSignListener(signView, signName);
    }

}
