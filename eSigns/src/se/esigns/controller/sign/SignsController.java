package se.esigns.controller.sign;

import java.util.Map;
import java.util.Set;

import se.esigns.model.commercial.Commercial;
import se.esigns.model.commercial.Policy;
import se.esigns.model.publisher.Statistics;
import se.esigns.model.publisher.CommercialStats;
import se.esigns.model.publisher.SignStats;
import se.esigns.model.sign.ISignAllocatorDatabase;
import se.esigns.model.sign.Sign;

public class SignsController {
    ISignAllocatorDatabase data;
    private boolean active = false;
    private int _SYNC_INTERVAL_SEC = 10;

    public SignsController(ISignAllocatorDatabase data) {
	this.data = data;
    }

    private Thread commercialsAllocator;

    public void startAllocation() {
	commercialsAllocator = new Thread(new Runnable() {
	    @Override
	    public void run() {
		// TODO Auto-generated method stub
		while (active) {
		    System.out.print("signs allocated");
		    allocate();
		    try {
			Thread.sleep(_SYNC_INTERVAL_SEC * 1000);
		    } catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		    }
		}
	    }
	});
	active = true;
	commercialsAllocator.start();
    }

    public void stopAllocation() {
	active = false;
    }

    public void allocate() {
	// Set<Sign> signs = data.getSigns();
	Map<String, Commercial> commercials = data.getCommercials();
	for (Sign s : data.getSigns().values()) {
	    data.clearCommercialsFromSign(s.getSignName());

	    processStats(commercials, s);
	}

	for (Commercial c : commercials.values()) {
	    if (c.expired()) {
		data.moveCommercialToExpired(c);
		continue;
	    }
	    Policy p = c.getPublishingPolicy();
	    for (String s : c.getSignesSet()) {

		if (p.timeToPublish()) {
		    if (c.isPushed())
			continue;
		    data.addCommercialToSign(c, s);
		}
	    }
	    p.publish();
	}

    }

    private void processStats(Map<String, Commercial> commercials, Sign s) {
	Statistics addedFrom = s.getStats();
	for (Map.Entry<String, CommercialStats> entryComm : addedFrom.commercialsPublished
		.entrySet()) {
	    String commercialId = entryComm.getKey();

	    CommercialStats stat = entryComm.getValue();
	    for (Map.Entry<String, SignStats> entrySign : stat.publishedInSigns
		    .entrySet()) {
		String signName = entrySign.getKey();
		SignStats t = entrySign.getValue();
		Commercial c = commercials.get(commercialId);
		if (c==null) continue;
		String publisher = c.getPublisherName();
		data.updatePublisherStatistics(publisher, commercialId,
			signName, t.signDisplayTime, t.signCharge);
	    }
	}
    }
}
