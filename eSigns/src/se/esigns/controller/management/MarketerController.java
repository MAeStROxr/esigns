package se.esigns.controller.management;

import java.util.Map;

import se.esigns.AppDatabase;
import se.esigns.model.commercial.Commercial;
import se.esigns.model.management.IMarketingDatabase;
import se.esigns.model.publisher.Publisher;
import se.esigns.model.sign.ISignsDatabaseListener;
import se.esigns.model.sign.Sign;
//import se.esigns.model.sign.SignPricing;
import se.esigns.view.management.MarketerView;

public class MarketerController {

    IMarketingDatabase marketingData;
    MarketerView marketerView;
    
    /**
     * 
     * @param username
     * @param password
     * @param toNotify
     * @param clientAppData
     */

    public MarketerController(IMarketingDatabase marketingData, MarketerView marketerView) {
	this.marketingData = marketingData;
	this.marketerView=marketerView;
	this.marketerView.registerController(this);
    }
    
 public void initUI() {
     marketerView.onInitUI();
    }
    /**
     * 
     * @param Sign
     */
    public void notifySignRemoved(Sign sign) {
	// marketingData.getRegisteredPublishers().forEach(pub->pub.notifySignRemoved(sign));
	Map<String, Publisher> loggedIn = marketingData.getLoggedInPublishers();
	for (Commercial c : sign.getCommercials()) {
	    marketingData.notifyPublisher(c.getPublisherName(),
		    "Sign removed: " + sign.getSignName()
			    + " you will no longer be charged for it.");
	}

    }

    /**
     * 
     * @param Sign
     * @param pricing
     */
    public void setSignPrice(String sign, double pricing) {
	marketingData.setSignPrice(sign, pricing);
	Map<String, Publisher> loggedIn = marketingData.getLoggedInPublishers();
	for (Commercial c : marketingData.getCommercials().values()) {
	    if (!loggedIn.containsKey(c.getPublisherName())) continue;
	    marketingData.notifyPublisher(c.getPublisherName(),
		    "New sign price set for: " + sign
			    + " the new price is: "+pricing);
	}
    }

    public void registerMarketingListener(ISignsDatabaseListener listener){
	marketingData.registerSignsListener(listener);
    }
    public void unregisterMarketingListener(ISignsDatabaseListener listener){
 	marketingData.unregisterSignsListener(listener);
     }
    public Map<String, Sign> getSigns() {
	return marketingData.getSigns();
    }
   
    

}