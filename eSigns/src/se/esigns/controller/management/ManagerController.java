package se.esigns.controller.management;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import se.esigns.model.management.IManagerDatabase;
import se.esigns.model.management.IManagerDatabaseListener;
import se.esigns.model.publisher.OfficialPublisher;
import se.esigns.model.publisher.Publisher;
import se.esigns.model.publisher.PublishingPrivilege;
import se.esigns.model.sign.AreaType;
import se.esigns.model.sign.Sign;
//import se.esigns.model.sign.SignPricing;
import se.esigns.view.management.ManagerView;

public class ManagerController {

    private IManagerDatabase data;
    private ManagerView view;
    private String managerUsername;
    private String managerPassword;

    /**
     * 
     * @param signsInchargeOf
     * @param registerOfficial
     */
    public ManagerController(IManagerDatabase managerData, ManagerView view) {
	// TODO: add manager registration
	this.data = managerData;
	this.view = view;
	this.view.registerController(this);

    }

    public void registerManagerListener(IManagerDatabaseListener listener) {
	this.data.registerManagerDataListener(listener, managerUsername,
		managerPassword);
    }

    public void unregisterManagerListener(IManagerDatabaseListener listener) {
	this.data.unregisterManagerDataListener(listener, managerUsername,
		managerPassword);
    }

    public void initUI() {
	view.onInitUI();

    }

    /**
     * 
     * @param username
     * @param password
     */
    public OfficialPublisher addOfficial(String username, String password) {
	if (!data.registerOfficial(username, password))
	    return null;
	OfficialPublisher op = (OfficialPublisher) data.getPublisher(username);

	return op;
    }

    public OfficialPublisher promoteToOfficial(String username) {
	if (!data.promoteOfficial(username))
	    return null;
	OfficialPublisher op = (OfficialPublisher) data.getPublisher(username);
	return op;
    }

    private PublishingPrivilege assignPrivilegeAll() {
	HashSet<AreaType> areaTypesPrivileged = new HashSet<AreaType>();
	areaTypesPrivileged.addAll(Arrays.asList(AreaType.values()));
	HashSet<String> areasPrivileged = new HashSet<String>();
	areasPrivileged.addAll(data.getAreas());
	PublishingPrivilege p = new PublishingPrivilege(areaTypesPrivileged,
		areasPrivileged);
	return p;
    }

    /**
     * 
     * @param OFFICIAL
     */
    public void setPrivilege(OfficialPublisher publisher,
	    PublishingPrivilege pp) {
	publisher.setPrivilege(pp);

    }

    /**
     * 
     * @param Sign
     */
    public boolean removeSign(Sign sign) {
	return data.removeSign(sign);
    }

    /**
     * 
     * @param Sign
     */
    public boolean addSign(Sign sign) {
	return data.addSign(sign);
    }

    public boolean addSign(String name, String location, AreaType areaType,
	    boolean canImage) {
	Sign s = new Sign(name, location, areaType, canImage);
	return addSign(s);
    }

    public boolean editSign(Sign sign) {
	return data.editSign(sign);
    }

    public Map<String, Sign> getSigns() {
	return data.getSigns();
    }

    public void removePublisher(String username2) {
	data.removePublisher(username2);
    }

    public List<Publisher> getPublishers() {
	return new ArrayList<>(data.getRegisteredPublishers().values());
    }

    public void demoteOfficial(String username2) {
	data.demoteOfficial(username2);
    }

    public void addLocationPrivilege(String location, String username) {
	data.addLocationPrivilege(location, username);
    }

    public void addAreaTypePrivilege(AreaType areaType, String username) {
	data.addAreaTypePrivilege(areaType, username);
    }

    public void removeLocationPrivilege(String location, String username) {
	data.removeLocationPrivilege(location, username);
    }

    public void removeAreaTypePrivilege(AreaType areaType, String username) {
	data.removeAreaTypePrivilege(areaType, username);
    }

    public Set<String> getLocations() {
	return data.getAreas();
    }
}