import static org.junit.Assert.*;

import org.junit.Test;

import se.esigns.model.sign.AreaType;
import se.esigns.model.sign.Sign;


public class testSignCommercial {

	@Test
	public void testSignCtors() {
		Sign mySign = new Sign("name1",2.5,"wtf?",AreaType.Urban,true);
		assertEquals("name1",mySign.getSignName());
		Sign copy1=new Sign(mySign);
		assertEquals(0,(int)(Math.abs(2.5-copy1.getPricing())));
		assertEquals(mySign,copy1);
		Sign sign2=new Sign("newName",copy1.getAreaName(),AreaType.Non_Urban,false);
		assertNotEquals(mySign.getAreaType(),sign2.getAreaType());
	}

	@Test
	public void testCommercialCtors() {
		return;
	}
		
}
